use std::io::{self, BufRead};

const M: u64 = 998244353;

fn pow(n: u64, p: u64) -> u64 {
  let mut q = p;
  let mut r = n;
  let mut result = 1;
  while q > 0 { 
    if q % 2 == 1 { 
      result = result * r % M;
    }   
    r = r * r % M;
    q >>= 1;
  }
  result
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (n, m) = (xs[0], xs[1]);
  if n == 2 || m == 2 {
    println!("0");
    return;
  }
  let mut prods: Vec<u64> = vec![0; (m + 1) as usize];
  prods[0] = 1;
  for i in 1..=m {
    prods[i as usize] = i * prods[(i-1) as usize] % M;
  }
  let mut ans: u64 = 0;
  for pivot in (n-1)..=m {
    let n_pairs = pivot - 1;
    let n_cands = (pivot - 2) as usize;
    let choose = (n - 3) as usize;
    let comb = (prods[n_cands] * pow(prods[n_cands - choose], M - 2) % M) * pow(prods[choose], M - 2) % M;
    ans = (ans + (n_pairs * comb % M) * pow(2, choose as u64) % M) % M;
  }
  println!("{}", ans);
}