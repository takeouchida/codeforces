use std::io::{self, BufRead};
use std::cmp::max;
use std::collections::BTreeSet;

fn dfs_upward(u: usize, es: &Vec<Vec<usize>>, cs: &Vec<bool>, visited: &mut BTreeSet<usize>, res: &mut Vec<i32>) -> i32 {
  visited.insert(u);
  let mut diff: i32 = if cs[u] { 1 } else { -1 };
  for &v in &es[u] {
    if !visited.contains(&v) {
      let d = dfs_upward(v, es, cs, visited, res);
      diff += max(0, d);
    }
  }
  res[u] = diff;
  diff
}

fn dfs_downward(u: usize, es: &Vec<Vec<usize>>, cs: &Vec<bool>, visited: &mut BTreeSet<usize>, res: &mut Vec<i32>, d_up: i32) {
  visited.insert(u);
  if d_up > 0 {
    res[u] += d_up;
  }
  let mut vs: Vec<usize> = Vec::new();
  for &v in &es[u] {
    if !visited.contains(&v) {
      vs.push(v);
    }
  }
  let sum: i32 = vs.iter().map(|&v| res[v]).filter(|&x| x > 0).sum();
  for &v in &vs {
    let d = sum - max(0, res[v]) + if cs[u] { 1 } else { -1 } + d_up;
    dfs_downward(v, es, cs, visited, res, d);
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let cs: Vec<bool> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse::<u8>().unwrap() == 1).collect();
  let mut es: Vec<Vec<usize>> = vec![vec![]; n];
  for _ in 0..(n-1) {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (u, v) = (xs[0] - 1, xs[1] - 1);
    es[u].push(v);
    es[v].push(u);
  }
  let mut res: Vec<i32> = vec![-1; n];
  let mut visited: BTreeSet<usize> = BTreeSet::new();
  dfs_upward(0, &es, &cs, &mut visited, &mut res);
  let mut visited: BTreeSet<usize> = BTreeSet::new();
  dfs_downward(0, &es, &cs, &mut visited, &mut res, 0);
  for &r in &res {
    print!("{} ", r);
  }
  println!();
}