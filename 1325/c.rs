use std::io::{self, BufRead};
use std::collections::BTreeMap;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let mut es: Vec<Vec<usize>> = vec![vec![]; n];
  let mut ixs: BTreeMap<(usize, usize), usize> = BTreeMap::new();
  for i in 0..(n-1) {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (u, v) = (xs[0] - 1, xs[1] - 1);
    es[u].push(v);
    es[v].push(u);
    ixs.insert((u, v), i);
  }
  let (i, ne) = es.iter().map(|e| e.len()).enumerate().max_by(|x, y| x.1.cmp(&y.1)).unwrap();
  let mut ans: Vec<usize> = vec![0; n-1];
  for j in 0..ne {
    let k = es[i][j];
    let l = if ixs.contains_key(&(i, k)) { ixs.get(&(i, k)).unwrap() } else { ixs.get(&(k, i)).unwrap() };
    ans[*l] = j + 1;
  }
  let mut j = ne;
  for k in 0..es.len() {
    if k == i {
      continue;
    }
    for &l in &es[k] {
      let m = if ixs.contains_key(&(k, l)) { ixs.get(&(k, l)).unwrap() } else { ixs.get(&(l, k)).unwrap() };
      if ans[*m] == 0 {
        ans[*m] = j + 1;
        j += 1;
      }
    }
  }
  for &a in &ans {
    println!("{}", a - 1);
  }
}