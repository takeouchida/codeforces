use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (u, v) = (xs[0], xs[1]);
  if u > v {
    println!("-1");
    return;
  }
  let mut ans: Vec<u64> = Vec::new();
  let a = v - u;
  if a % 2 != 0 {
    println!("-1");
    return;
  }
  if a == 0 {
    if u != 0 {
      ans.push(u);
    }
  } else {
    let b = a >> 1;
    if u ^ b == u + b {
      ans.push(u + b);
    } else {
      ans.push(u);
      ans.push(b);
    }
    ans.push(b);
  }
  println!("{}", ans.len());
  if !ans.is_empty() {
    for a in ans {
      print!("{} ", a);
    }
    println!();
  }
}