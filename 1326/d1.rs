use std::io::{self, BufRead};
use std::collections::{HashMap, HashSet};
use std::cmp::max;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let s: Vec<char> = lines.next().unwrap().unwrap().chars().collect();
    let t: Vec<char> = s.iter().rev().cloned().collect();
    let n = s.len();
    let mut h: HashMap<&[char], HashSet<(usize, usize)>> = HashMap::new();
    for i in 0..n {
      for j in (i+1)..=n {
        match h.get_mut(&s[i..j]) {
          None => { h.insert(&s[i..j], vec![(i, j)].into_iter().collect()); },
          Some(v) => { v.insert((i, j)); },
        }
      }
    }
    for i in 0..n {
      for j in (i+1)..=n {
        match h.get_mut(&t[i..j]) {
          None => { h.insert(&t[i..j], vec![(n - i, n - j)].into_iter().collect()); },
          Some(v) => { v.insert((n - i, n - j)); },
        }
      }
    }
    let mut ans1 = None;
    for v in h.values() {
      for (i, j) in v {
        if *i == 0 && *j <= n / 2 && v.contains(&(n, n - *j)) {
          match ans1 {
            None => { ans1 = Some(j); },
            Some(k) => { ans1 = Some(max(j, k)); },
          }
        }
      }
    }
    let (lo, hi) = match ans1 {
      None => (0, n),
      Some(k) => (*k, n - *k),
    };
    let mut ans2 = None;
    for v in h.values() {
      for (i, j) in v {
        if (*i == lo && *j <= hi && *i < *j && v.contains(&(*j, *i))) || (lo <= *i && *j == hi && *i < *j && v.contains(&(*j, *i))) {
          match ans2 {
            None => { ans2 = Some((i, j)); },
            Some((k, l)) => {
              if l - k < j - i {
                ans2 = Some((i, j));
              }
            }
          }
        }
      }
    }
    let mut ans = String::new();
    if let Some(&k) = ans1 {
      for i in 0..k {
        ans.push(s[i]);
      }
    }
    if let Some((&i, &j)) = ans2 {
      for k in i..j {
        ans.push(s[k]);
      }
    }
    if let Some(&k) = ans1 {
      for i in 0..k {
        ans.push(s[n-k+i]);
      }
    }
    println!("{}", ans);
  }
}