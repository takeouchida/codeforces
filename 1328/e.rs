use std::io::{self, BufRead};
use std::collections::BTreeSet;

fn dfs(u: usize, d: usize, es: &Vec<BTreeSet<usize>>, depths: &mut Vec<usize>, parents: &mut Vec<usize>, visited: &mut BTreeSet<usize>) {
  visited.insert(u);
  depths[u] = d;
  for &v in &es[u] {
    if !visited.contains(&v) {
      parents[v] = u;
      dfs(v, d + 1, es, depths, parents, visited);
    }
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (n, m) = (xs[0], xs[1]);
  let mut es: Vec<BTreeSet<usize>> = vec![BTreeSet::new(); n];
  for _ in 0..(n-1) {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (u, v) = (xs[0] - 1, xs[1] - 1);
    es[u].insert(v);
    es[v].insert(u);
  }
  let mut vss: Vec<Vec<usize>> = Vec::new();
  for _ in 0..m {
    let mut xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse::<usize>().unwrap() - 1).collect();
    xs.remove(0);
    vss.push(xs);
  }
  let mut depths: Vec<usize> = vec![0; n];
  let mut parents: Vec<usize> = vec![0; n];
  let mut visited: BTreeSet<usize> = BTreeSet::new();
  dfs(0, 0, &es, &mut depths, &mut parents, &mut visited);
  for i in 0..vss.len() {
    let mut vs: Vec<(usize, usize)> = vss[i].iter().map(|&j| (j, depths[j])).collect();
    vs.sort_by(|x, y| y.1.cmp(&x.1));
    let mut j = 1;
    let mut p = parents[vs[0].0];
    let mut ok = true;
    for d in (0..vs[0].1).rev() {
      while j < vs.len() && (vs[j].0 == p || es[p].contains(&vs[j].0)) {
        j += 1;
      }
      if j < vs.len() && vs[j].1 > d {
        ok = false;
        break;
      }
      p = parents[p];
    }
    println!("{}", if ok { "YES" } else { "NO" });
  }
}