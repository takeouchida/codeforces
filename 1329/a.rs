use std::io::{self, BufRead};
use std::cmp::min;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (n, m) = (xs[0], xs[1] as usize);
  let xs: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  if (n as usize) < m - 1 + xs[m-1] as usize {
    println!("-1");
    return;
  }
  let sum: u64 = xs.iter().sum();
  if sum < n {
    println!("-1");
    return;
  }
  let mut ls: Vec<u64> = Vec::new();
  let mut p: u64 = 0;
  for i in 0..m {
    let k: u64 = n - xs[m-1] - (m - i - 1) as u64;
    p = min(k, p);
    ls.push(p);
    p += xs[i];
  }
  for i in 0..m {
    print!("{} ", ls[i] + 1);
  }
  println!();
}