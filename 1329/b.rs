use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (d, m) = (xs[0], xs[1]);
    let l = (64 - d.leading_zeros()) as usize;
    let mut ns: Vec<u64> = vec![0; l];
    ns[l-1] = (d - (2 as u64).pow(l as u32 - 1) + 1) % m;
    for i in (0..(l-1)).rev() {
      let p = (2 as u64).pow(i as u32);
      ns[i] = (p * (ns[i+1] + 1) + ns[i+1]) % m;
    }
    println!("{}", ns[0]);
  }
}