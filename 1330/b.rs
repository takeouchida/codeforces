use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).map(|x: usize| x - 1).collect();
    let mut p: usize = 0;
    let mut bs: Vec<bool> = vec![false; n];
    let mut ixs: Vec<usize> = Vec::new();
    for i in 0..n {
      bs[xs[i]] = true;
      while p < n && bs[p] {
        p += 1;
      }
      if i + 1 == p {
        ixs.push(p);
      }
    }
    let mut p: usize = 0;
    let mut bs: Vec<bool> = vec![false; n];
    let mut jxs: Vec<usize> = Vec::new();
    for i in (0..n).rev() {
      bs[xs[i]] = true;
      while p < n && bs[p] {
        p += 1;
      }
      if n - i == p {
        jxs.push(p);
      }
    }
    jxs.reverse();
    let mut i: usize = 0;
    let mut j: usize = 0;
    let mut ans: Vec<(usize, usize)> = Vec::new();
    while i < ixs.len() && j < jxs.len() {
      if ixs[i] + jxs[j] == n {
        ans.push((ixs[i], jxs[j]));
        i += 1;
        j += 1;
      } else if ixs[i] + jxs[j] < n {
        i += 1;
      } else {
        j += 1;
      }
    }
    println!("{}", ans.len());
    for (i, j) in ans {
      println!("{} {}", i, j);
    }
  }
}