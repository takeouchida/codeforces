use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let a: Vec<i64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let mut sums: Vec<(i64, usize)> = vec![(0, 0); n + 1];
  for i in 0..n {
    sums[i+1] = (sums[i].0 + a[i], i + 1);
  }
  sums.sort_by(|x, y| if x.0 == y.0 { x.1.cmp(&y.1) } else { x.0.cmp(&y.0) });
  let mut lo: usize = 0;
  let mut sum: i64 = sums[0].0;
  let mut ixs: Vec<(usize, usize)> = Vec::new();
  for i in 1..=n {
    if sum != sums[i].0 {
      ixs.push((lo, i));
      lo = i;
      sum = sums[i].0
    }
    if i == n {
      ixs.push((lo, n + 1));
    }
  }
  let mut counts: Vec<i64> = (1..=(n as i64)).collect();
  for (lo, hi) in &ixs {
    if hi - lo <= 1 {
      continue;
    }
    for i in (lo+1)..*hi {
      counts[sums[i].1 - 1] = (sums[i].1 - sums[i-1].1 - 1) as i64;
    }
  }
  let ans: i64 = counts.iter().sum();
  println!("{:?}", ans);
}