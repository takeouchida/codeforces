use std::io::{self, BufRead};
use std::cmp::min;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let mut s = String::new();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let mut ls: Vec<u64> = vec![0; n];
    let mut bs: Vec<u64> = vec![0; n];
    let mut cs: Vec<u64> = vec![0; n];
    let mut total: u64 = 0;
    for i in 0..n {
      let xs: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
      ls[i] = xs[0];
      bs[i] = xs[1];
      if i > 0 {
        cs[i] = if bs[i-1] < ls[i] { ls[i] - bs[i-1] } else { 0 };
        total += cs[i];
      }
    }
    cs[0] = if bs[n-1] < ls[0] { ls[0] - bs[n-1] } else { 0 };
    total += cs[0];
    let mut ans: u64 = u64::max_value();
    for i in 0..n {
      ans = min(ans, total - cs[i] + ls[i]);
    }
    s.push_str(format!("{}\n", ans).as_str());
  }
  print!("{}", s);
}