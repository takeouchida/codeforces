use std::io::{self, BufRead};
use std::cmp::{min, max};

const NUM_AL: usize = 26;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).map(|x: usize| x - 1).collect();
    let mut pref: Vec<Vec<usize>> = vec![vec![0; n + 1]; NUM_AL];
    for i in 0..n {
      for c in 0..NUM_AL {
        pref[c][i+1] = pref[c][i] + if xs[i] == c { 1 } else { 0 };
      }
    }
    let mut ans = pref.iter().map(|ps| ps[n]).max().unwrap();
    for l in 0..n {
      for r in l..(n+1) {
        let cntin = (0..NUM_AL).map(|c| pref[c][r] - pref[c][l]).max().unwrap();
        let cntout = (0..NUM_AL).map(|c| min(pref[c][l], pref[c][n] - pref[c][r])).max().unwrap();
        ans = max(ans, cntin + 2 * cntout);
      }
    }
    println!("{}", ans);
  }
}