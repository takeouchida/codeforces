use std::io::{self, BufRead};
use std::cmp::max;

const NUM_AL: usize = 200;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).map(|x: usize| x - 1).collect();
    let mut iss: Vec<Vec<usize>> = vec![vec![]; NUM_AL];
    let mut pref: Vec<Vec<usize>> = vec![vec![0; n + 1]; NUM_AL];
    for i in 0..n {
      iss[xs[i]].push(i);
      for c in 0..NUM_AL {
        pref[c][i+1] = pref[c][i] + if xs[i] == c { 1 } else { 0 };
      }
    }
    let mut ans = pref.iter().map(|ps| ps[n]).max().unwrap();
    for c in 0..NUM_AL {
      let nc = iss[c].len();
      for i in 0..(nc / 2) {
        let l = iss[c][i] + 1;
        let r = iss[c][nc-i-1];
        let cntin = (0..NUM_AL).map(|c| pref[c][r] - pref[c][l]).max().unwrap();
        let cntout = (i + 1) * 2;
        ans = max(ans, cntin + cntout);
      }
    }
    println!("{}", ans);
  }
}