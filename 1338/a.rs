use std::io::{self, BufRead};
use std::cmp::max;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let mut xs: Vec<i32> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let mut ans: usize = 0;
    for i in 1..n {
      if xs[i-1] > xs[i] {
        let mut count: usize = 0;
        let mut d = xs[i-1] - xs[i];
        while d > 0 {
          d >>= 1;
          count += 1;
        }
        ans = max(ans, count);
        xs[i] = xs[i-1];
      }
    }
    println!("{}", ans);
  }
}