use std::io::{self, BufRead};
use std::collections::BTreeSet;

fn dfs_min(es: &Vec<Vec<usize>>, visited: &mut BTreeSet<usize>, masks: &mut BTreeSet<usize>, n: usize) -> usize {
  visited.insert(n);
  let mut nodes: BTreeSet<usize> = BTreeSet::new();
  for &e in &es[n] {
    if !visited.contains(&e) {
      let m = dfs_min(es, visited, masks, e);
      nodes.insert(m);
    }
  }
  let mut res: usize = 0;
  while res < nodes.len() {
    if nodes.contains(&res) {
      res += 1;
    } else {
      break;
    }
  }
  for &node in &nodes {
    masks.insert(node ^ res);
  }
  res
}

fn dfs_max_sub(es: &Vec<Vec<usize>>, visited: &mut BTreeSet<usize>, n: usize) -> (usize, usize) {
  visited.insert(n);
  let mut counts: Vec<usize> = Vec::new();
  for &e in &es[n] {
    if !visited.contains(&e) {
      let (c, _) = dfs_max_sub(es, visited, e);
      counts.push(c);
    }
  }
  let zeros = counts.iter().filter(|&&c| c == 0).count();
  let pos = counts.len() - zeros;
  let res = counts.iter().sum::<usize>() + pos + (if zeros == 0 { 0 } else { 1 });
  (res, zeros)
}

fn dfs_max(es: &Vec<Vec<usize>>, visited: &mut BTreeSet<usize>, n: usize) -> usize {
  if es[n].len() == 1 {
    visited.insert(n);
    let (c, z) = dfs_max_sub(es, visited, es[n][0]);
    c + (if z == 0 { 1 } else { 0 })
  } else {
    dfs_max_sub(es, visited, n).0
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let mut es: Vec<Vec<usize>> = vec![vec![]; n];
  for _ in 0..(n-1) {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (u, v) = (xs[0] - 1, xs[1] - 1);
    es[u].push(v);
    es[v].push(u);
  }
  let mut visited = BTreeSet::new();
  let mut masks = BTreeSet::new();
  dfs_min(&es, &mut visited, &mut masks, 0);
  let ans_min = masks.len();
  let mut visited = BTreeSet::new();
  let ans_max = dfs_max(&es, &mut visited, 0);
  println!("{} {}", ans_min, ans_max);
}