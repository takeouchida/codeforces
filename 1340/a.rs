use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let mut xs: Vec<(usize, usize)> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).enumerate().collect();
    xs.sort_by(|a, b| a.1.cmp(&b.1));
    let mut lo = xs[0].0;
    let mut hi = n;
    let mut beg = 0;
    let mut ok = true;
    for i in 0..n {
      if i >= beg + hi - lo {
        hi = lo;
        lo = xs[i].0;
        beg = i;
      }
      if xs[i].0 != lo + i - beg {
        ok = false;
        break;
      }
    }
    println!("{}", if ok { "YES" } else { "NO" });
  }
}