use std::io::{self, BufRead};
use std::collections::BTreeSet;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (_, m) = (xs[0], xs[1]);
  let ds: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (g, r) = (xs[0], xs[1]);
  let mut visited: BTreeSet<usize> = BTreeSet::new();
  let mut i: usize = 0;
  let mut rest: usize = g;
  let mut t: usize = 0;
  let mut ok = true;
  while i < m - 1 {
    let d = ds[i+1] - ds[i];
    if d <= rest {
      t += d;
      rest -= d;
      i += 1;
    } else if i > 0 {
      let d = ds[i] - ds[i-1];
      if d <= rest {
        t += d;
        rest -= d;
        i -= 1;
      } else {
        if visited.contains(&i) {
          ok = false;
          break;
        } else {
          visited.insert(i);
        }
        t += rest + r;
        rest = g;
      }
    } else {
      if visited.contains(&i) {
        ok = false;
        break;
      } else {
        visited.insert(i);
      }
      t += rest + r;
      rest = g;
    }
  }
  println!("{} {}", ok, t);
}