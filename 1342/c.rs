use std::io::{self, BufRead};
use std::cmp::max;

fn gcd(x: u64, y: u64) -> u64 {
  let (mut a, mut b) = if x < y {
    (y, x)
  } else {
    (x, y)
  };
  while b != 0 {
    let r = a % b;
    a = b;
    b = r;
  }
  a
}

fn lcm(x: u64, y: u64) -> u64 {
  x * y / gcd(x, y)
}

fn count(x: u64, a: u64, b: u64, l: u64) -> u64 {
  let d = x / l;
  let m = x % l;
  let c = max(a, b);
  (l - c) * d + if m < c { 0 } else { m - c + 1 }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (a, b, q) = (xs[0], xs[1], xs[2]);
    let m = lcm(a, b) as usize;
    for _ in 0..q {
      let xs: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
      let (l, r) = (xs[0] - 1, xs[1]);
      let x = count(l, a, b, m as u64);
      let y = count(r, a, b, m as u64);
      print!("{} ", y - x);
    }
    println!();
  }
}