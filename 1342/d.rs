use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (n, k) = (xs[0], xs[1]);
  let mut ms: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  ms.sort();
  let cs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let mut ds: Vec<usize> = vec![0; k];
  let mut j: usize = k;
  let mut count: usize = 0;
  for i in (0..n).rev() {
    while ms[i] < j {
      ds[j-1] = count;
      j -= 1;
    }
    if ms[i] == j {
      count += 1;
    }
  }
  for i in 0..j {
    ds[i] = count;
  }
  let ans = (0..k).map(|i| (ds[i] + cs[i] - 1) / cs[i]).max().unwrap();
  let mut tss: Vec<Vec<usize>> = vec![vec![]; ans];
  for i in (0..n).rev() {
    tss[i%ans].push(ms[i]);
  }
  println!("{}", ans);
  for i in 0..ans {
    print!("{}", tss[i].len());
    for j in 0..tss[i].len() {
      print!(" {}", tss[i][j]);
    }
    println!();
  }
}