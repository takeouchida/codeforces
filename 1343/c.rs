use std::io::{self, BufRead};
use std::cmp::max;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let xs: Vec<i64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let mut temp = xs[0];
    let mut ans = 0;
    for i in 1..n {
      if (temp < 0 && xs[i] > 0) || (temp > 0 && xs[i] < 0) {
        ans += temp;
        temp = xs[i];
      } else {
        temp = max(temp, xs[i]);
      }
    }
    ans += temp;
    println!("{}", ans);
  }
}