use std::io::{self, BufRead};
use std::collections::{BinaryHeap, BTreeSet};
use std::cmp::{Ordering, min};

#[derive(PartialEq, Eq, Debug, Copy, Clone, Default, Hash)]
struct Move(usize, usize);

impl PartialOrd for Move {
  fn partial_cmp(&self, other: &Move) -> Option<Ordering> {
    other.0.partial_cmp(&self.0)
  }
}

impl Ord for Move {
  fn cmp(&self, other: &Move) -> Ordering {
    other.0.cmp(&self.0)
  }
}

fn dijkstra(es: &Vec<Vec<usize>>, a: usize) -> Vec<usize> {
  let mut ans: Vec<usize> = vec![usize::max_value(); es.len()];
  let mut visited: BTreeSet<usize> = BTreeSet::new();
  let mut queue: BinaryHeap<Move> = BinaryHeap::new();
  queue.push(Move(0, a));
  while let Some(Move(d, u)) = queue.pop() {
    visited.insert(u);
    ans[u] = min(ans[u], d);
    for &v in &es[u] {
      if !visited.contains(&v) {
        queue.push(Move(d + 1, v));
      }
    }
  }
  ans
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (n, m, a, b, c) = (xs[0], xs[1], xs[2] - 1, xs[3] - 1, xs[4] - 1);
    let ps: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let mut es: Vec<Vec<usize>> = vec![vec![]; n];
    for _ in 0..m {
      let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
      let (u, v) = (xs[0] - 1, xs[1] - 1);
      es[u].push(v);
      es[v].push(u);
    }
    let das = dijkstra(&es, a);
    let dbs = dijkstra(&es, b);
    let dcs = dijkstra(&es, c);
    let (_, x) = (0..n).map(|x| (das[x] + 2 * dbs[x] + dcs[x], x)).min_by(|a, b| a.0.cmp(&b.0)).unwrap();
    println!("{:?} {:?} {:?} {}", das, dbs, dcs, x);
  }
}