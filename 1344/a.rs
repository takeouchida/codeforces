use std::io::{self, BufRead};
use std::collections::HashSet;

fn md(a: i32, b: i32) -> i32 {
  if a >= 0 {
    a % b
  } else {
    (b - (-a) % b) % b
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let xs: Vec<i32> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let ys: HashSet<i32> = xs.iter().enumerate().map(|(i, x)| md(i as i32 + x, n as i32)).collect();
    println!("{}", if ys.len() == n { "YES" } else { "NO" });
  }
}