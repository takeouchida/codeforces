use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (n, k) = (xs[0], xs[1]);
    let mut sum: u64 = 0;
    let mut m: u64 = 1;
    for _ in 0..4 {
      sum += m;
      m *= k;
    }
    let mut q = n / sum;
    for _ in 0..4 {
      print!("{} ", q);
      q *= k;
    }
    println!();
  }
}