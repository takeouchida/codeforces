use std::io::{self, BufRead};
use std::collections::HashSet;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (n, k) = (xs[0], xs[1]);
    let ys: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let mut s: HashSet<usize> = ys.iter().cloned().collect();
    if s.len() > k {
      println!("-1");
      continue;
    }
    let mut zs: Vec<usize> = Vec::new();
    let mut i: usize = 0;
    while i < n {
      if zs.len() < k {
        if k - zs.len() <= s.len() && !s.contains(&ys[i]) {
          let z = s.iter().cloned().next().unwrap();
          zs.push(z);
          s.remove(&z);
        } else {
          zs.push(ys[i]);
          s.remove(&ys[i]);
          i += 1;
        }
      } else {
        if zs[zs.len()-k] == ys[i] {
          zs.push(ys[i]);
          i += 1;
        } else {
          zs.push(zs[zs.len()-k]);
        }
      }
    }
    println!("{}", zs.len());
    for z in zs {
      print!("{} ", z);
    }
    println!();
  }
}