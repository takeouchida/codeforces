use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (n, k) = (xs[0], xs[1]);
    let mut cs: Vec<char> = lines.next().unwrap().unwrap().chars().collect();
    cs.sort();
    if cs[0] != cs[k-1] || n == k {
      println!("{}", cs[k-1]);
    } else if cs[k] == cs[n-1] {
      let mut ans = String::new();
      ans.push(cs[0]);
      let count = (n - k - 1) / k + 1;
      for _ in 0..count {
        ans.push(cs[k]);
      }
      println!("{}", ans);
    } else {
      let mut ans = String::new();
      ans.push(cs[0]);
      let count = n - k;
      for i in 0..count {
        ans.push(cs[k+i]);
      }
      println!("{}", ans);
    }
  }
}