use std::io::{self, BufRead};
use std::collections::HashMap;

pub fn find_prime_factors(n: u64) -> HashMap<u64, u64> {
  let mut res = HashMap::new();
  let mut i = 2;
  let mut m = n;
  while i * i <= m {
    while m % i == 0 {
      let val = 1 + res.get(&i).unwrap_or(&0);
      res.insert(i, val);
      m /= i;
    }
    i += 1
  }
  if m != 1 {
    let val = 1 + res.get(&m).unwrap_or(&0);
    res.insert(m, val);
  }
  res
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let xs: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let mut pss: HashMap<u64, Vec<u64>> = HashMap::new();
  for i in 0..xs.len() {
    let fs = find_prime_factors(xs[i]);
    for (k, v) in fs {
      match pss.get_mut(&k) {
        Some(ps) => {
          ps.push(v);
        },
        None => {
          pss.insert(k, vec![v]);
        }
      }
    }
  }
  for ps in pss.values_mut() {
    ps.sort();
  }
  let mut ans: u64 = 1;
  for (k, ps) in &pss {
    if ps.len() == n {
      for _ in 0..ps[1] {
        ans *= k
      }
    } else if ps.len() == n - 1 {
      for _ in 0..ps[0] {
        ans *= k;
      }
    }
  }
  println!("{:?}", ans);
}