use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (n, k) = (xs[0], xs[1]);
    if n % 2 == 0 {
      if k % 2 == 0 {
        if n >= k {
          println!("YES");
          print!("{}", n - k + 1);
          for _ in 1..k {
            print!(" 1");
          }
          println!();
        } else {
          println!("NO");
        }
      } else {
        if n >= k * 2 {
          println!("YES");
          print!("{}", n - k * 2 + 2);
          for _ in 1..k {
            print!(" 2");
          }
          println!();
        } else {
          println!("NO");
        }
      }
    } else {
      if k % 2 == 0 {
        println!("NO");
      } else {
        if n >= k {
          println!("YES");
          print!("{}", n - k + 1);
          for _ in 1..k {
            print!(" 1");
          }
          println!();
        } else {
          println!("NO")
        }
      }
    }
  }
}