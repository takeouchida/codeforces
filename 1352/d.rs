use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let mut i: usize = 0;
    let mut j: usize = n - 1;
    let mut acc_a: usize = 0;
    let mut acc_b: usize = 0;
    let mut a: usize = 0;
    let mut b: usize = 0;
    let mut is_alice = true;
    let mut k: usize = 1;
    while i <= j {
      if is_alice {
        a += xs[i];
        if b < a {
          is_alice = false;
          acc_a += a;
          b = 0;
          if i < j {
            k += 1;
          }
        }
        i += 1;
      } else {
        b += xs[j];
        if a < b {
          is_alice = true;
          acc_b += b;
          a = 0;
          if i < j {
            k += 1;
          }
        }
        j -= 1;
      }
    }
    if is_alice {
      acc_a += a;
    } else {
      acc_b += b;
    }
    println!("{} {} {}", k, acc_a, acc_b);
  }
}