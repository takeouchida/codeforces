use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let xs: Vec<u16> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let mut ys: Vec<u32> = vec![0; n + 1];
    for i in 0..xs.len() {
      ys[i+1] = ys[i] + xs[i] as u32;
    }
    let mut ws: Vec<u16> = vec![0; n + 1];
    for i in 0..xs.len() {
      ws[xs[i] as usize] += 1;
    }
    let mut bs: Vec<bool> = vec![false; n + 1];
    let mut ans: u16 = 0;
    for i in 0..xs.len() {
      for j in (i + 2)..(xs.len() + 1) {
        let s = ys[j] - ys[i];
        if s <= n as u32 && !bs[s as usize] {
          ans += ws[s as usize];
          bs[s as usize] = true;
        }
      }
    }
    println!("{}", ans);
  }
}