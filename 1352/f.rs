use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (n0, n1, n2) = (xs[0], xs[1], xs[2]);
    let mut s = String::new();
    if n0 > 0 || n1 > 0 {
      s.push('0');
    } else {
      s.push('1');
    }
    for _ in 0..n0 {
      s.push('0');
    }
    let n01 = if n1 == 0 { 0 } else if n1 % 2 == 0 { n1 - 1 } else { n1 };
    for i in 0..n01 {
      s.push(if i % 2 == 0 { '1' } else { '0' });
    }
    for _ in 0..n2 {
      s.push('1');
    }
    if n1 > 1 && n1 % 2 == 0 {
      s.push('0');
    }
    println!("{}", s);
  }
}