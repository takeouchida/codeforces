use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    if n < 4 {
      println!("-1");
      continue;
    }
    let k = (n - 3) / 2;
    for i in 0..k {
      print!("{} ", (k - i - 1) * 2 + 5);
    }
    print!("2 4 1 3 ");
    for i in 0..((n - 4) / 2) {
      print!("{} ", i * 2 + 6);
    }
    println!();
  }
}