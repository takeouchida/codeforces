use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: u64 = lines.next().unwrap().unwrap().parse().unwrap();
    let mut ans: u64 = 0;
    for i in 1..(n/2+1) {
      ans += i * i * 8;
    }
    println!("{}", ans);
  }
}