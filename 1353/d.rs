use std::io::{self, BufRead};
use std::collections::BinaryHeap;
use std::cmp::Ordering;

#[derive(PartialEq, Eq, Debug, Copy, Clone)]
struct Range {
  lo: usize,
  hi: usize,
}

impl Range {
  fn new(lo: usize, hi: usize) -> Range {
    Range { lo, hi }
  }
}

impl PartialOrd for Range {
  fn partial_cmp(&self, other: &Range) -> Option<Ordering> {
    if self.hi - self.lo == other.hi - other.lo {
      other.lo.partial_cmp(&self.lo)
    } else {
      (self.hi - self.lo).partial_cmp(&(other.hi - other.lo))
    }
  }
}

impl Ord for Range {
  fn cmp(&self, other: &Range) -> Ordering {
    if self.hi - self.lo == other.hi - other.lo {
      other.lo.cmp(&self.lo)
    } else {
      (self.hi - self.lo).cmp(&(other.hi - other.lo))
    }
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let mut xs: Vec<usize> = vec![0; n];
    let mut q: BinaryHeap<Range> = BinaryHeap::new();
    q.push(Range::new(0, n - 1));
    let mut i: usize = 1;
    while !q.is_empty() {
      let r = q.pop().unwrap();
      let m = (r.hi + r.lo) / 2;
      xs[m] = i;
      if r.lo < m {
        q.push(Range::new(r.lo, m - 1));
      }
      if m < r.hi {
        q.push(Range::new(m + 1, r.hi));
      }
      i += 1;
    }
    for x in xs {
      print!("{} ", x);
    }
    println!();
  }
}