use std::io::{self, BufRead};
use std::cmp::{min, max};

fn dp(xss: &Vec<Vec<i64>>, yss: &mut Vec<Vec<(i64, i64)>>, n: usize, m: usize, x: usize, y: usize, z: usize) {
  if x < m - 1 && y < n - 1 {
    let u = yss[y+1][x].1 + if xss[y][x] < yss[y+1][x].0 {
      (yss[y+1][x].0 - xss[y][x] - 1) * z as i64
    } else {
      xss[y][x] - yss[y+1][x].0 + 1
    };
    let l = yss[y][x+1].1 + if xss[y][x] < yss[y][x+1].0 {
      (yss[y][x+1].0 - xss[y][x] - 1) * z as i64
    } else {
      xss[y][x] - yss[y][x+1].0 + 1
    };
    if u < l {
      yss[y][x] = if xss[y][x] < yss[y+1][x].0 {
        (xss[y][x], u)
      } else {
        (yss[y+1][x].0 - 1, u)
      };
    } else {
      yss[y][x] = if xss[y][x] < yss[y][x+1].0 {
        (xss[y][x], l)
      } else {
        (yss[y][x+1].0 - 1, l)
      }
    }
  }
  else if x < m - 1 && n - 1 <= y {
    let l = yss[y][x+1].1 + if xss[y][x] < yss[y][x+1].0 {
      (yss[y][x+1].0 - xss[y][x] - 1) * z as i64
    } else {
      xss[y][x] - yss[y][x+1].0 + 1
    };
    yss[y][x] = if xss[y][x] < yss[y][x+1].0 {
      (xss[y][x], l)
    } else {
      (yss[y][x+1].0 - 1, l)
    }
  }
  else if y < n - 1 && m - 1 <= x {
    let u = yss[y+1][x].1 + if xss[y][x] < yss[y+1][x].0 {
      (yss[y+1][x].0 - xss[y][x] - 1) * z as i64
    } else {
      xss[y][x] - yss[y+1][x].0 + 1
    };
    yss[y][x] = if xss[y][x] < yss[y+1][x].0 {
      (xss[y][x], u)
    } else {
      (yss[y+1][x].0 - 1, u)
    };
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (n, m) = (xs[0], xs[1]);
    let mut xss: Vec<Vec<i64>> = Vec::new();
    for _ in 0..n {
      let xs: Vec<i64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
      xss.push(xs);
    }
    let mut yss: Vec<Vec<(i64, i64)>> = vec![vec![(0, 0); m]; n];
    yss[n-1][m-1] = (xss[n-1][m-1], 0);
    let p = min(m, n);
    let q = max(m, n);
    let d = if m < n { n - m } else { m - n };
    for i in 1..p {
      for j in 0..(i+1) {
        let x = m - 1 - j;
        let y = n - (i - j) - 1;
        let z = i;
        dp(&xss, &mut yss, n, m, x, y, z);
      }
    }
    for i in 0..d {
      for j in 0..p {
        let x = if m < n { j } else { q - i - j - 2 };
        let y = if m < n { q - i - j - 2 } else { j };
        let z = p + i;
        dp(&xss, &mut yss, n, m, x, y, z);
      }
    }
    for i in 1..p {
      for j in 0..(p-i) {
        let x = j;
        let y = p - i - 1 - j;
        let z = q + i - 1;
        dp(&xss, &mut yss, n, m, x, y, z);
      }
    }
    println!("{}", yss[0][0].1);
  }
}