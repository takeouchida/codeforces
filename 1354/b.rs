use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let cs: Vec<char> = lines.next().unwrap().unwrap().chars().collect();
    let mut xs: Vec<(usize, usize)> = Vec::new();
    let mut c = cs[0];
    let mut count: usize = 1;
    for i in 1..cs.len() {
      if cs[i] != c {
        xs.push((c as usize - '1' as usize, count));
        c = cs[i];
        count = 1;
      } else {
        count += 1;
      }
    }
    xs.push((c as usize - '1' as usize, count));
    let o = (1..(xs.len()-1)).filter(|i| xs[*i].0 != xs[*i-1].0 && xs[*i].0 != xs[*i+1].0 && xs[*i-1].0 != xs[*i+1].0).map(|i| xs[i]).min_by(|x, y| x.1.cmp(&y.1));
    println!("{}", match o { None => 0, Some((_, x)) => x + 2 });
  }
}