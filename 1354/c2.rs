use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let x: i32 = lines.next().unwrap().unwrap().parse().unwrap();
    let ans: f64 = (std::f64::consts::FRAC_PI_2 * (1.0 - 1.0 / x as f64)).tan();
    println!("{}", ans);
  }
}