use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<i32> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (a, b, c, d) = (xs[0], xs[1], xs[2], xs[3]);
  let mut count: i32 = 0;
  for l in a..(b+1) {
    let lo = l + b;
    let hi = l + c;
    let c = if lo < c {
      if hi < c {
        0
      } else if hi <= d {
        let x = hi - c;
        x * (x + 1) / 2
      } else {
        let x = d - c;
        x * (x + 1) / 2 + (hi - d) * (d - c + 1)
      }
    } else if lo <= d {
      if hi < d {
        let x = lo - c;
        let y = hi - c;
        (x + y) * (y - x + 1) / 2
      } else {
        let x = lo - c;
        let y = hi - c;
        (x + y) * (y - x + 1) / 2 + (hi - d) * (d - c + 1)
      }
    } else {
      (hi - lo) * (d - c)
    };
    println!("{} {} {} {}", l, lo, hi, c);
    count += c;
  }
  println!("{}", count);
}