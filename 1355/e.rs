use std::io::{self, BufRead};
use std::cmp::min;

pub fn lower_bound<T: Ord>(vec: &[T], val: T) -> usize {
  let mut lo = 0;
  let mut hi = vec.len();
  while hi - lo > 0 {
    let p = (hi + lo) / 2;
    if vec[p] < val {
      lo = p + 1;
    } else {
      hi = p;
    }
  }
  return lo;
}

fn calc_by_move(hs: &Vec<u64>, accs: &Vec<u64>, h: u64, a: u64, r: u64, m: u64) -> u64 {
  let n = hs.len();
  let i = lower_bound(hs, h);
  let insuff = h * i as u64 - accs[i];
  let excess = (accs[n] - accs[i]) - h * (n - i) as u64;
  if insuff < excess {
    m * insuff + r * (excess - insuff)
  } else {
    m * excess + a * (insuff - excess)
  }
}

fn calc_by_add_rem(hs: &Vec<u64>, accs: &Vec<u64>, h: u64, a: u64, r: u64) -> u64 {
  let n = hs.len();
  let i = lower_bound(hs, h);
  let insuff = h * i as u64 - accs[i];
  let excess = (accs[n] - accs[i]) - h * (n - i) as u64;
  insuff * a + excess * r
}

fn search<F>(mut lo: u64, mut hi: u64, f: F) -> u64 where F: Fn(u64) -> u64 {
  while hi - lo >= 3 {
    let l = lo + (hi - lo) / 3;
    let r = hi - (hi - lo) / 3;
    if f(l) < f(r) {
      hi = r;
    } else {
      lo = l;
    }
  }
  (lo..hi).map(f).min().unwrap()
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (n, a, r, m) = (xs[0], xs[1] as u64, xs[2] as u64, xs[3] as u64);
  let mut hs: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  hs.sort();
  let mut accs: Vec<u64> = vec![0];
  for i in 0..n {
    accs.push(accs[i] + hs[i]);
  }
  let min1 = search(hs[0], hs[n-1] + 1, |h| calc_by_move(&hs, &accs, h, a, r, m));
  let min2 = search(hs[0], hs[n-1] + 1, |h| calc_by_add_rem(&hs, &accs, h, a, r));
  println!("{}", min(min1, min2));
}