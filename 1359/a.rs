use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let k: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..k {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (n, m, k) = (xs[0], xs[1], xs[2]);
    let unit = n / k;
    let ans = if m <= unit {
      m
    } else {
      let other = k - 1;
      let second = (m - unit + other - 1) / other;
      unit - second
    };
    println!("{}", ans);
  }
}