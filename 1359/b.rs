use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (n, x, y) = (xs[0], xs[2], xs[3]);
    let mut css: Vec<Vec<usize>> = Vec::new();
    for _ in 0..n {
      let mut s: Vec<char> = lines.next().unwrap().unwrap().chars().collect();
      s.push('*');
      let mut cs: Vec<usize> = Vec::new();
      let mut count: usize = 0;
      for c in s {
        if c == '.' {
          count += 1;
        } else if count > 0 {
          cs.push(count);
          count = 0;
        }
      }
      css.push(cs);
    }
    let mut ans: usize = 0;
    for cs in css {
      for c in cs {
        if x * 2 < y {
          ans += x * c;
        } else {
          ans += y * (c / 2) + x * (c % 2);
        }
      }
    }
    println!("{}", ans);
  }
}