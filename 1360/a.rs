use std::io::{self, BufRead};
use std::cmp::max;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (a, b) = if xs[0] < xs[1] { (xs[0], xs[1]) } else { (xs[1], xs[0]) };
    println!("{}", max(b * b, 4 * a * a));
  }
}