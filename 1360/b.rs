use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0.. t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let mut ss: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    ss.sort();
    let ans = (1..n).map(|i| ss[i] - ss[i-1]).min().unwrap();
    println!("{}", ans);
  }
}