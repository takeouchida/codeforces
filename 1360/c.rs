use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let mut xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let even = xs.iter().filter(|&x| *x % 2 == 0).count();
    if even % 2 == 0 {
      println!("Yes");
    } else {
      xs.sort();
      let ok = (1..n).any(|i| xs[i] - xs[i-1] == 1);
      println!("{}", if ok { "Yes" } else { "No" });
    }
  }
}