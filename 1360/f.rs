use std::io::{self, BufRead};

fn acceptable(s: &Vec<char>, t: &Vec<char>) -> bool {
  (0..s.len()).filter(|i| s[*i] != t[*i]).count() <= 1
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let mut css: Vec<Vec<char>> = Vec::new();
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (n, m) = (xs[0], xs[1]);
    for _ in 0..n {
      let cs: Vec<char> = lines.next().unwrap().unwrap().chars().collect();
      css.push(cs);
    }
    let mut ok = false;
    let mut ans: Vec<char> = Vec::new();
    for i in 0..n {
      if css.iter().all(|cs| acceptable(cs, &css[i])) {
        ok = true;
        ans = css[i].clone();
        break;
      }
      for j in 0..m {
        let c = css[i][j];
        for k in 0..26 {
          let d = ('a' as u8 + k as u8) as char;
          css[i][j] = d;
          if css.iter().all(|cs| acceptable(cs, &css[i])) {
            ok = true;
            ans = css[i].clone();
            break;
          }
        }
        css[i][j] = c;
        if ok {
          break;
        }
      }
      if ok {
        break;
      }
    }
    if ok {
      for c in ans {
        print!("{}", c);
      }
      println!();
    } else {
      println!("-1");
    }
  }
}