use std::io::{self, BufRead};

fn gcd(x: usize, y: usize) -> usize {
  let (mut a, mut b) = if x < y {
    (y, x)
  } else {
    (x, y)
  };
  while b != 0 {
    let r = a % b;
    a = b;
    b = r;
  }
  a
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (n, m, a, b) = (xs[0], xs[1], xs[2], xs[3]);
    let d = gcd(n, m);
    let r = n / d;
    let c = m / d;
    let u = b / r;
    let ok = b % r == 0 && a % c == 0 && u == a / c;
    if ok {
      println!("Yes");
      for i in 0..n {
        for j in 0..m {
          let k = i % d;
          let l = j % d;
          print!("{}", if (l + d - k) % d < u { 1 } else { 0 });
        }
      println!();
      }
    } else {
      println!("No");
    }
  }
}