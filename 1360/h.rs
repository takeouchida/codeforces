use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (n, m) = (xs[0], xs[1]);
    let total: u64 = (2 as u64).pow(m as u32);
    let mut xs: Vec<u64> = Vec::new();
    for _ in 0..n {
      let x = lines.next().unwrap().unwrap().chars().fold(0 as u64, |acc, x| (acc << 1) + if x == '0' { 0 } else { 1 });
      xs.push(x);
    }
    xs.push(total);
    xs.sort();
    let mut ys: Vec<(u64, u64)> = Vec::new();
    let mut l: u64 = 0;
    let mut acc: u64 = 0;
    for i in 0..(n+1) {
      if l == xs[i] {
        l += 1;
      } else {
        acc += xs[i] - l;
        ys.push((xs[i] - 1, acc));
        l = xs[i] + 1;
      }
    }
    let target = (total - n as u64 + 1) / 2;
    let (i, acc) = ys.iter().find(|(_, a)| target <= *a).unwrap();
    let ans = i - (acc - target);
    let iter: Vec<u8> = (0..m).map(|k| if (1 << (m - k - 1)) & ans != 0 { '1' as u8 } else { '0' as u8 }).collect();
    println!("{}", String::from_utf8(iter).unwrap());
  }
}