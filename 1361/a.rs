use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (_, m) = (xs[0], xs[1]);
  let mut es: Vec<(usize, usize)> = Vec::new();
  for _ in 0..m {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    es.push((xs[0] - 1, xs[1] - 1));
  }
  let vs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  for (u, v) in es {
    if vs[u] == vs[v] {
      println!("-1");
      return;
    }
  }
  let mut ixs: Vec<(usize, usize)> = vs.into_iter().enumerate().collect();
  ixs.sort_by(|a, b| a.1.cmp(&b.1));
  for (i, _) in ixs {
    print!("{} ", i + 1);
  }
  println!();
}