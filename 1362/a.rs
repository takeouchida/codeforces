use std::io::{self, BufRead};
use std::mem::swap;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (mut a, mut b) = (xs[0], xs[1]);
    if b < a {
      swap(&mut a, &mut b);
    }
    if b % a != 0 {
      println!("-1");
      continue;
    }
    let mut q: u64 = b / a;
    let mut count: u64 = 0;
    let mut ok = true;
    while 1 < q {
      if q % 2 == 1 {
        ok = false;
        break;
      }
      q >>= 1;
      count += 1;
    }
    if ok {
      println!("{}", (count + 2) / 3);
    } else {
      println!("-1");
    }
  }
}