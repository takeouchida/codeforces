use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let mut ss: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    ss.sort();
    let mut ans: i32 = -1;
    for k in 1..1024 {
      let mut ok = true;
      for i in 0..n {
        let j = ss[i] ^ k;
        if ss.binary_search(&j).is_err() {
          ok = false;
          break;
        }
      }
      if ok {
        ans = k as i32;
        break;
      }
    }
    println!("{}", ans);
  }
}