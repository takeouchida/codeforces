use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let mut counts: Vec<u64> = vec![0, 1];
  for i in 1..61 {
    counts.push(counts[i] * 2 + i as u64 + 1);
  }
  for _ in 0..t {
    let mut total: u64 = 0;
    let n: u64 = lines.next().unwrap().unwrap().parse().unwrap();
    for i in 0..61 {
      let k: u64 = 1 << i;
      if n & k != 0 {
        total += counts[i] + i as u64 + 1;
      }
    }
    println!("{}", total);
  }
}