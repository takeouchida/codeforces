use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let x = xs[1];
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let even = xs.iter().filter(|x| **x % 2 == 0).count();
    let odd = xs.len() - even;
    if odd == 0 {
      println!("No");
      continue
    }
    let odd_taken = (odd - 1) / 2 * 2 + 1;
    let ok = if x % 2 != 0 {
      x <= odd_taken || x - odd_taken <= even
    } else {
      (x - 1 <= odd_taken && 1 <= even) || (odd_taken < x - 1 && x - 1 - odd_taken <= even)
    };
    println!("{}", if ok { "Yes" } else { "No" });
  }
}