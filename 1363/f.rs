use std::io::{self, BufRead};
use std::mem::swap;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let _: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let mut s = lines.next().unwrap().unwrap();
    let t: Vec<char> = lines.next().unwrap().unwrap().chars().collect();
    let mut count: usize = 0;
    let mut ok = true;
    for i in 0..t.len() {
      let c = t[i];
      if s.chars().next().unwrap() == c {
        let mut u = String::from(&s[1..s.len()]);
        swap(&mut s, &mut u);
      } else if let Some(j) = s.chars().rev().position(|x| x == c) {
        count += j + 1;
        let mut u = String::with_capacity(s.len() - 1);
        u.push_str(&s[(s.len()-j)..s.len()]);
        u.push_str(&s[0..(s.len()-j-1)]);
        swap(&mut s, &mut u);
      } else {
        ok = false;
        break;
      }
    }
    if ok {
      println!("{}", count);
    } else {
      println!("-1");
    }
  }
}