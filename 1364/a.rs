use std::io::{self, BufRead};
use std::cmp::min;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (n, x) = (xs[0], xs[1] as u64);
    let ys: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let ok = ys.iter().any(|y| y % x != 0);
    if !ok {
      println!("-1");
      continue;
    }
    let sum: u64 = ys.iter().sum();
    if sum % x != 0 {
      println!("{}", n);
      continue;
    }
    let mut m: u64 = sum % x;
    let mut left: usize = 0;
    while m == 0 && left < n {
      m = (m + (x - ys[left] % x)) % x;
      left += 1;
    }
    m = sum % x;
    let mut right: usize = 0;
    while m == 0 && right < n {
      m = (m + (x - ys[n-right-1] % x)) % x;
      right += 1;
    }
    println!("{}", n - min(left, right));
  }
}