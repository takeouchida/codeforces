use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let ss: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let mut ts: Vec<usize> = vec![ss[0]];
    for i in 1..(n-1) {
      if ss[i-1] < ss[i] && ss[i] > ss[i+1] {
        ts.push(ss[i]);
      } else if ss[i-1] == ss[i] && ss[i] != ss[i+1] {
        ts.push(ss[i]);
      } else if ss[i-1] > ss[i] && ss[i] < ss[i+1] {
        ts.push(ss[i]);
      }
    }
    ts.push(ss[n-1]);
    println!("{}", ts.len());
    for t in ts {
      print!("{} ", t);
    }
    println!();
  }
}