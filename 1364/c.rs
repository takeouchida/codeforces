use std::io::{self, BufRead};
use std::cmp::min;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  if xs[0] > 1 {
    println!("-1");
    return;
  }
  let mut ys: Vec<usize> = Vec::new();
  ys.push(if xs[0] == 0 { 1 } else { 0 });
  let mut x: usize = xs[0];
  let mut j: usize = 0;
  let mut max: usize = xs[0];
  for i in 1..n {
    if x == xs[i] {
      continue;
    }
    if xs[i] == ys[j] {
      println!("-1");
      return;
    }
    if xs[j] < ys[j] {
      if ys[j] + i - j < xs[i] {
        println!("-1");
        return;
      }
      for k in 0..(i-j-1) {
        ys.push(min(ys[j] + k + 1, xs[i] - 1));
      }
      ys.push(xs[j]);
      max = min(ys[j] + i - j, xs[i] - 1);
    } else {
      if max + i - j + 1 < xs[i] {
        println!("-1");
        return;
      }
      for k in 0..(i-j-1) {
        ys.push(min(max + k + 1, ys[j] + i - j));
      }
      ys.push(xs[j]);
      max = min(max + i - j, ys[j] + i - j);
    }
    x = xs[i];
    j = i;
  }
  println!("{:?}", ys);
}