use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let xs: Vec<i32> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let mut ys: Vec<i32> = vec![-1; n];
  let mut zs: Vec<i32> = Vec::new();
  for i in 1..n {
    if xs[i-1] != xs[i] {
      ys[i] = xs[i-1];
      zs.push(xs[i-1]);
    }
  }
  zs.push(xs[n-1]);
  let mut k: usize = 0;
  let mut next: i32 = 0;
  for i in 0..n {
    if ys[i] >= 0 {
      continue;
    }
    while next == zs[k] {
      next += 1;
      if k < zs.len() - 1 {
        k += 1;
      }
    }
    ys[i] = next;
    next += 1;
  }
  for y in ys {
    print!("{} ", y);
  }
  println!();
}