use std::io::{self, BufRead};
use std::collections::{BTreeMap, BTreeSet};
use std::mem::swap;

fn dfs(es: &Vec<Vec<usize>>, visited: &mut BTreeMap<usize, usize>, path: &mut Vec<usize>, v: usize, k: usize) -> bool {
  if let Some(u) = visited.get(&v) {
    let ok = path.len() - u <= k;
    if ok {
      let mut ans = path[*u..(path.len())].to_vec();
      swap(&mut ans, path);
    }
    return ok;
  }
  visited.insert(v, visited.len());
  path.push(v);
  for u in &es[v] {
    if path.len() > 1 && path[path.len()-2] == *u {
      continue;
    }
    if dfs(es, visited, path, *u, k) {
      return true;
    }
  }
  path.pop();
  visited.remove(&v);
  false
}

fn dfs_color(es: &Vec<Vec<usize>>, cs: &mut Vec<i32>, v: usize) {
  if cs[v] >= 0 {
    return;
  }
  let used: BTreeSet<i32> = es[v].iter().map(|u| cs[*u]).filter(|c| *c >= 0).collect();
  let mut c: i32 = 0;
  while c < es.len() as i32 && used.contains(&c) {
    c += 1;
  }
  cs[v] = c;
  for &u in &es[v] {
    dfs_color(es, cs, u);
  }
}

fn color(es: &Vec<Vec<usize>>, k: usize) -> Vec<usize> {
  let mut cs: Vec<i32> = vec![-1; es.len()];
  let start = (0..es.len()).map(|i| (i, es[i].len())).max_by(|x, y| x.1.cmp(&y.1)).unwrap().0;
  cs[start] = 1;
  for &u in &es[start] {
    dfs_color(es, &mut cs, u);
  }
  let n = (k + 1) / 2;
  return cs.iter().enumerate().filter(|(_, c)| **c == 0).map(|(i, _)| i).take(n).collect();
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (n, m, k) = (xs[0], xs[1], xs[2]);
  let mut es: Vec<Vec<usize>> = vec![vec![]; n];
  for _ in 0..m {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (u, v) = (xs[0] - 1, xs[1] - 1);
    es[u].push(v);
    es[v].push(u);
  }
  let mut visited: BTreeMap<usize, usize> = BTreeMap::new();
  let mut path: Vec<usize> = vec![];
  let cycle_exists = dfs(&es, &mut visited, &mut path, 0, k);
  if cycle_exists {
    println!("2\n{}", path.len());
    let mut s = String::new();
    for v in path {
      s.push_str(&format!("{} ", v + 1));
    }
    println!("{}", s);
    return;
  }
  let vs = color(&es, k);
  println!("1");
  let mut s = String::new();
  for v in vs {
    s.push_str(&format!("{} ", v + 1));
  }
  println!("{}", s);
}