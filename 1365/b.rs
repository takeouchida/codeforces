use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let ys: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let zeros = ys.iter().filter(|y| **y == 0).count();
    let is_sorted = (0..(n-1)).all(|i| xs[i] <= xs[i+1]);
    let ok = (0 < zeros && zeros < n) || is_sorted;
    println!("{}", if ok { "Yes" } else { "No" });
  }
}