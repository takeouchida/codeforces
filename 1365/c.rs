use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let mut ys: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let mut ixs: Vec<(usize, usize)> = xs.iter().cloned().enumerate().collect();
  ixs.sort_by(|a, b| a.1.cmp(&b.1));
  for i in 0..ys.len() {
    ys[i] = ixs[ys[i]-1].0;
  }
  let mut counts: Vec<usize> = vec![0; n];
  for i in 0..ys.len() {
    let j = (ys[i] + n - i) % n;
    counts[j] += 1;
  }
  let ans = counts.iter().max().unwrap();
  println!("{}", ans);
}