use std::io::{self, BufRead};
use std::collections::BTreeMap;
use std::mem::swap;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let ys: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    if n % 2 == 1 && xs[n/2] != ys[n/2] {
      println!("No");
      continue;
    }
    let mut m: BTreeMap<(usize, usize), usize> = BTreeMap::new();
    for i in 0..(n / 2) {
      let j = n - i - 1;
      let mut xi = xs[i];
      let mut xj = xs[j];
      if xj < xi {
        swap(&mut xi, &mut xj);
      }
      let v = 1 + m.get(&(xi, xj)).unwrap_or(&0);
      m.insert((xi, xj), v);
    }
    let mut ok = true;
    for i in 0..(n / 2) {
      let j = n - i - 1;
      let mut yi = ys[i];
      let mut yj = ys[j];
      if yj < yi {
        swap(&mut yi, &mut yj);
      }
      let o = m.get(&(yi, yj)).cloned();
      match o {
        None => {
          ok = false;
          break;
        },
        Some(1) => {
          m.remove(&(yi, yj));
        },
        Some(v) => {
          m.insert((yi, yj), v - 1);
        }
      }
    }
    println!("{}", if ok { "Yes" } else { "No" });
  }
}