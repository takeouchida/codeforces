use std::io::{self, BufRead};
use std::cmp::min;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (a, b) = if xs[0] < xs[1] { (xs[0], xs[1]) } else { (xs[1], xs[0]) };
    let ans = if a * 2 <= b {
      min(a, b / 2)
    } else {
      let c = b - a;
      let d = a - c;
      d / 3 * 2 + c + (if d % 3 == 2 { 1 } else { 0 })
    };
    println!("{}", ans);
  }
}