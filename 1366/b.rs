use std::io::{self, BufRead};
use std::cmp::{min, max};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (x, m) = (xs[1], xs[2]);
    let mut lo = x;
    let mut hi = x;
    for _ in 0..m {
      let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
      let (l, r) = (xs[0], xs[1]);
      if lo <= r && l <= hi {
        lo = min(lo, l);
        hi = max(hi, r);
      }
    }
    println!("{}", hi - lo + 1);
  }
}