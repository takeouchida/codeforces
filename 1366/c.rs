use std::io::{self, BufRead};
use std::cmp::min;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (n, m) = (xs[0], xs[1]);
    let mut xss: Vec<Vec<u8>> = Vec::new();
    for _ in 0..n {
      let xs: Vec<u8> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
      xss.push(xs);
    }
    let ixs: usize = (n + m - 1) / 2;
    let mut count: usize = 0;
    for i in 0..ixs {
      let c = if n < m {
        if i < n {
          let len = i + 1;
          let zeros = (0..len).filter(|&k| xss[k][len-k-1] == 0).count() + (0..len).filter(|&k| xss[n-k-1][m-(len-k-1)-1] == 0).count();
          let ones = 2 * len - zeros;
          min(zeros, ones)
        } else {
          let zeros = (0..n).filter(|&k| xss[k][i+1-k] == 0).count() + (0..n).filter(|&k| xss[n-k-1][m-(i+1-k)-1] == 0).count();
          let ones = 2 * n - zeros;
          min(zeros, ones)
        }
      } else {
        if i < m {
          let len = i + 1;
          let zeros = (0..len).filter(|&k| xss[k][len-k-1] == 0).count() + (0..len).filter(|&k| xss[n-k-1][m-(len-k-1)-1] == 0).count();
          let ones = 2 * len - zeros;
          min(zeros, ones)
        } else {
          let zeros = (0..m).filter(|&k| xss[i-k][k] == 0).count() + (0..m).filter(|&k| xss[n-(i-k)-1][m-k-1] == 0).count();
          let ones = 2 * m - zeros;
          min(zeros, ones)
        }
      };
      count += c;
    }
    println!("{}", count);
  }
}