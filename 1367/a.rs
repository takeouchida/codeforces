use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let s: String = lines.next().unwrap().unwrap();
    let s = s.as_bytes();
    let mut t: Vec<u8> = Vec::new();
    t.push(s[0]);
    for i in 0..(s.len()/2) {
      t.push(s[i*2+1]);
    }
    println!("{}", String::from_utf8(t).unwrap());
  }
}