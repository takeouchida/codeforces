use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let even: i32 = xs.iter().filter(|&x| *x % 2 == 0).count() as i32;
    let odd: i32 = n as i32 - even;
    let ok = (n % 2 == 0 && even == odd) || (n % 2 == 1 && even - odd == 1);
    if !ok {
      println!("-1");
    } else {
      let bad_even = xs.iter().enumerate().filter(|(i, &x)| i % 2 == 1 && x % 2 == 0).count();
      println!("{}", bad_even);
    }
  }
}