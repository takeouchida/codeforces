use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (n, k) = (xs[0], xs[1]);
    let ts: Vec<char> = lines.next().unwrap().unwrap().chars().collect();
    let mut ys: Vec<bool> = vec![false; n];
    for i in 0..n {
      if ts[i] == '0' {
        continue;
      }
      for j in 0..(k*2+1) {
        if k <= i + j && i + j < n + k {
          ys[i+j-k] = true;
        }
      }
    }
    let mut count: usize = 0;
    for i in 0..n {
      if !ys[i] {
        count += 1;
        for j in 0..k {
          if i + j + 1 < n {
            ys[i+j+1] = true;
          }
        }
      }
    }
    println!("{}", count);
  }
}