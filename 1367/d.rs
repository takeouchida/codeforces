use std::io::{self, BufRead};
use std::collections::BTreeMap;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let q: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..q {
    let cs: Vec<i32> = lines.next().unwrap().unwrap().chars().map(|c| c as i32 - 'a' as i32).collect();
    let mut tally: BTreeMap<i32, i32> = BTreeMap::new();
    for c in cs {
      tally.insert(c, 1 + tally.get(&c).unwrap_or(&0));
    }
    let tally: Vec<(i32, i32)> = tally.iter().rev().map(|(&k, &v)| (k, v)).collect();
    let m: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let xs: Vec<i32> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let mut ys: Vec<Option<i32>> = vec![None; m];
    for (k, v) in &tally {
      let ixs: Vec<usize> = (0..m).filter(|i| ys[*i].is_none()).filter(|i| {
        let sum: i32 = ys.iter().enumerate().filter(|(_, o)| o.is_some()).map(|(j, _)| (*i as i32 - j as i32).abs()).sum();
        sum == xs[*i]
      }).collect();
      if ixs.len() <= *v as usize {
        for i in ixs {
          ys[i] = Some(*k);
        }
      }
    }
    let s: Vec<u8> = ys.iter().filter(|y| y.is_some()).map(|y| y.unwrap() as u8 + 'a' as u8).collect();
    println!("{}", String::from_utf8(s).unwrap());
  }
}