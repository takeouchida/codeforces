use std::io::{self, BufRead};
use std::mem::swap;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (mut a, mut b, n) = (xs[0], xs[1], xs[2]);
    if a > b {
      swap(&mut a, &mut b);
    }
    let mut count: usize = 0;
    while b <= n  {
      a += b;
      swap(&mut a, &mut b);
      count += 1;
    }
    println!("{}", count);
  }
}