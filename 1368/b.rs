use std::io::{self, BufRead};
use std::collections::BTreeMap;

pub fn find_prime_factors(n: u64) -> BTreeMap<u64, u64> {
  let mut res = BTreeMap::new();
  let mut i = 2;
  let mut m = n;
  while i * i <= m {
    while m % i == 0 {
      let val = 1 + res.get(&i).unwrap_or(&0);
      res.insert(i, val);
      m /= i;
    }
    i += 1
  }
  if m != 1 {
    let val = 1 + res.get(&m).unwrap_or(&0);
    res.insert(m, val);
  }
  res
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: u64 = lines.next().unwrap().unwrap().parse().unwrap();
  let ps = find_prime_factors(n);
  let mut counts: Vec<u64> = vec![1; 10];
  let mut i: usize = 0;
  for (&k, &v) in &ps {
    for _ in 0..v {
      counts[i] *= k;
      i = (i + 1) % 10;
    }
  }
  let cs: Vec<u8> = "codeforces".as_bytes().to_vec();
  let mut ans: Vec<u8> = Vec::new();
  for i in 0..counts.len() {
    for _ in 0..counts[i] {
      ans.push(cs[i]);
    }
  }
  println!("{}", String::from_utf8(ans).unwrap());
}