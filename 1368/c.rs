use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: i32 = lines.next().unwrap().unwrap().parse().unwrap();
  let mut i = 0;
  let mut j = 0;
  while i < n {
    let mut a = i;
    let mut d = 4 + 2 * i;
    j = 0;
    while a < n {
      a += d;
      d += 4;
      j += 1;
    }
    if a == n {
      break;
    }
    i += 1;
  }
  j += 1;
  let mut ans: Vec<(i32, i32)> = Vec::new();
  for y in 0..j {
    for x in 0..(2*(y+1)) {
      ans.push((x + j - y - 1, y));
    }
  }
  for y in 0..i {
    for x in 0..(2*j+1) {
      ans.push((x + y, y + j));
    }
  }
  for y in 0..j {
    for x in 0..(2*(j-y)) {
      ans.push((i + y + x, i + j + y));
    }
  }
  println!("{}", ans.len());
  for (x, y) in ans {
    println!("{} {}", x, y);
  }
}