use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let cs: Vec<char> = lines.next().unwrap().unwrap().chars().collect();
    let mut state: u8 = 0;
    let mut rear_zeros: usize = 0;
    let mut rear_ones: usize = 0;
    for i in 0..n {
      let k = n - i - 1;
      if cs[k] == '0' {
        rear_zeros += 1;
        if state == 0 {
          state = 1;
        }
      } else {
        if state != 0 {
          break;
        }
        rear_ones += 1;
      }
    }
    state = 0;
    let mut front_zeros: usize = 0;
    let mut front_ones: usize = 0;
    for i in 0..(n - rear_zeros - rear_ones) {
      if cs[i] == '0' {
        front_zeros += 1;
        if state == 0 {
          state = 1;
        }
      } else if state != 0 {
        break;
      } else {
        front_ones += 1;
      }
    }
    let mut ans: Vec<u8> = Vec::new();
    if front_ones == 0 {
      for _ in 0..front_zeros {
        ans.push(0 as u8 + '0' as u8);
      }
    }
    if front_ones != 0 || front_zeros != 0 {
      if rear_zeros != 0 {
        ans.push(0 as u8 + '0' as u8);
      }
    } else {
      for _ in 0..rear_zeros {
        ans.push(0 as u8 + '0' as u8);
      }
    }
    for _ in 0..rear_ones {
      ans.push(1 as u8 + '0' as u8);
    }
    println!("{}", String::from_utf8(ans).unwrap());
  }
}