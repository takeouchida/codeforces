use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let k = xs[1];
    let mut xs: Vec<i64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let mut ws: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    xs.sort_by(|a, b| b.cmp(&a));
    ws.sort();
    let mut ans: i64 = 0;
    for i in 0..k {
      if ws[i] == 1 {
        ans += 2 * xs[i];
      } else {
        ans += xs[i];
      }
    }
    let mut j: usize = k - 1;
    for i in 0..k {
      if ws[i] > 1 {
        j += ws[i] - 1;
        ans += xs[j];
      }
    }
    println!("{}", ans);
  }
}