use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let mut xs: Vec<i64> = Vec::new();
  for _ in 0..t {
    let x: i64 = lines.next().unwrap().unwrap().parse().unwrap();
    xs.push(x);
  }
  let m = xs.iter().max().unwrap();
  let mut memo: Vec<i64> = vec![0; *m as usize + 1];
  memo[3] = 4;
  memo[4] = 4;
  memo[5] = 12;
  for i in 6..(*m as usize + 1) {
    if i % 3 == 0 {
      memo[i] = (memo[i-2] + 4 * memo[i-3] + 4 * memo[i-4] + 4) % 1000000007;
    } else {
      memo[i] = (memo[i-1] + 2 * memo[i-2]) % 1000000007;
    }
  }
  for i in 0..t {
    println!("{}", memo[xs[i] as usize]);
  }
}