use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let _: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let evens: Vec<(usize, usize)> = xs.iter().cloned().enumerate().filter(|x| x.1 % 2 == 0).collect();
    let odds: Vec<(usize, usize)> = xs.iter().cloned().enumerate().filter(|x| x.1 % 2 == 1).collect();
    if evens.len() % 2 == 0 {
      if evens.len() > 0 {
        for i in 1..(evens.len()/2) {
          println!("{} {}", evens[i*2].0 + 1, evens[i*2+1].0 + 1);
        }
        for i in 0..(odds.len()/2) {
          println!("{} {}", odds[i*2].0 + 1, odds[i*2+1].0 + 1);
        }
      } else {
        for i in 0..(evens.len()/2) {
          println!("{} {}", evens[i*2].0 + 1, evens[i*2+1].0 + 1);
        }
        for i in 1..(odds.len()/2) {
          println!("{} {}", odds[i*2].0 + 1, odds[i*2+1].0 + 1);
        }
      }
    } else {
      for i in 0..(evens.len()/2) {
        println!("{} {}", evens[i*2+1].0 + 1, evens[i*2+2].0 + 1);
      }
      for i in 0..(odds.len()/2) {
        println!("{} {}", odds[i*2+1].0 + 1, odds[i*2+2].0 + 1);
      }
    }
  }
}