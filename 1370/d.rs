use std::io::{self, BufRead};
use std::collections::BTreeSet;
use std::cmp::{min, max};

fn find(ps: &mut Vec<usize>, cs: &mut Vec<usize>, x: usize) -> usize {
  if ps[x] == x {
    return x;
  } else {
    let p = find(ps, cs, ps[x]);
    cs[p] += cs[x];
    cs[x] = 0;
    ps[x] = p;
    return p;
  }
}

fn unite(ps: &mut Vec<usize>, rs: &mut Vec<usize>, cs: &mut Vec<usize>, x: usize, y: usize) {
  let x = find(ps, cs, x);
  let y = find(ps, cs, y);
  if x == y {
    return;
  }
  if rs[x] < rs[y] {
    cs[y] += cs[x];
    cs[x] = 0;
    ps[x] = y;
  } else {
    cs[x] += cs[y];
    cs[y] = 0;
    ps[y] = x;
    if rs[x] == rs[y] {
      rs[x] += 1;
    }
  }
}

fn is_same(ps: &mut Vec<usize>, cs: &mut Vec<usize>, x: usize, y: usize) -> bool {
  find(ps, cs, x) == find(ps, cs, y)
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (n, k) = (xs[0], xs[1]);
  let mut zs: Vec<(usize, usize)> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).enumerate().collect();
  zs.sort_by(|x, y| x.1.cmp(&y.1));
  let xs: Vec<_> = zs.iter().map(|x| x.0).collect();
  let mut ps: Vec<usize> = (0..n).collect();
  let mut rs: Vec<usize> = vec![0; n];
  let mut cs: Vec<usize> = vec![1; n];
  let mut visited: BTreeSet<usize> = BTreeSet::new();
  let mut count: usize = 0;
  let mut i: usize = 0;
  while count < k {
    visited.insert(xs[i]);
    count += 1;
    // if xs[i] > 0 && visited.contains(&(xs[i]-1)) {
    //   unite(&mut ps, &mut rs, &mut cs, xs[i] - 1, xs[i]);
    //   if cs[ps[xs[i]]] % 2 == 0 {
    //     count -= 1;
    //   }
    // }
    // if visited.contains(&(xs[i]+1)) {
    //   unite(&mut ps, &mut rs, &mut cs, xs[i] + 1, xs[i]);
    //   if cs[ps[xs[i]]] % 2 == 0 {
    //     count -= 1;
    //   }
    // }
    i += 1;
  }
  // println!("{} {:?} {:?} {:?}", i, xs, ps, cs);
  let mut x: usize = 0;
  let mut y: usize = 0;
  let mut i: usize = 0;
  println!("{:?}", visited);
  for v in visited {
    if i % 2 == 0 {
      x = max(x, zs[v].1);
    } else {
      y = max(y, zs[v].1);
    }
    i += 1;
  }
  println!("{}", min(x, y));
}