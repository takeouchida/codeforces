use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (n, r) = (xs[0], xs[1]);
    let ans = if n > r {
      (1 + r) * r / 2
    } else {
      1 + (n - 1) * n / 2
    };
    println!("{}", ans);
  }
}