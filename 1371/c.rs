use std::io::{self, BufRead};
use std::cmp::min;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (a, b, n, m) = (xs[0], xs[1], xs[2], xs[3]);
    let c = min(a, b);
    let ok = m <= c && m + n <= a + b;
    println!("{}", if ok { "Yes" } else { "No" });
  }
}