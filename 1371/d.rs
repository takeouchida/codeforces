use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (n, k) = (xs[0], xs[1]);
    println!("{}", if k % n == 0 { 0 } else { 2 });
    let mut m: Vec<Vec<bool>> = vec![vec![false; n]; n];
    for i in 0..k {
      let j = i % n;
      let l = (i + (i / n)) % n;
      m[j][l] = true;
    }
    for i in 0..n {
      let s: Vec<u8> = m[i].iter().map(|&b| if b { '1' as u8 } else { '0' as u8 }).collect();
      println!("{}", String::from_utf8(s).unwrap());
    }
  }
}