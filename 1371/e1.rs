use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (n, p) = (xs[0], xs[1]);
  let mut xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  xs.sort();
  let mut ans: Vec<usize> = Vec::new();
  for x in xs[0]..xs[n-1] {
    let mut y = x;
    let mut i = 0;
    let mut t = 0;
    let mut ok = true;
    while i < n {
      if xs[i] <= y {
        i += 1;
      } else if (i - t) % p == 0 {
        ok = false;
        break;
      } else {
        t += 1;
        y += 1;
      }
    }
    ok = ok && n - t < p;
    if ok {
      ans.push(x);
    }
  }
  println!("{}", ans.len());
  for i in 0..ans.len() {
    print!("{} ", ans[i]);
  }
  println!();
}