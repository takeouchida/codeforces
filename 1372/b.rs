use std::io::{self, BufRead};

pub fn find_divisors(n: u64) -> Vec<u64> {
  let mut res = Vec::new();
  let mut i = 1;
  while i * i <= n {
    if n % i == 0 {
      res.push(i);
      if i != n / i {
        res.push(n / i);
      }
    }
    i += 1
  }
  res.sort();
  res
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: u64 = lines.next().unwrap().unwrap().parse().unwrap();
    let ds = find_divisors(n);
    let a = n / ds[1];
    let b = n - a;
    println!("{} {}", a, b);
  }
}