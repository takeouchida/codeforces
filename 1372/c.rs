use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let mut parity = xs[0] == 1;
    let mut count: usize = if parity { 0 } else { 1 };
    for i in 1..n {
      let p = i + 1 == xs[i];
      if (p && !parity) || (!p && parity) {
        count += 1;
      }
      parity = p;
    }
    let ans = if count == 0 { 0 } else if count <= 2 { 1 } else { 2 };
    println!("{}", ans);
  }
}