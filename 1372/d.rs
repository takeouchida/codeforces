use std::io::{self, BufRead};
use std::cmp::min;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let xs: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let mut evens: Vec<u64> = vec![0; n / 2 + 2];
  let mut odds: Vec<u64> = vec![0; n / 2 + 1];
  for i in 1..(n/2+2) {
    evens[i] = evens[i-1] + xs[(i-1)*2];
  }
  for i in 1..(n/2+1) {
    odds[i] = odds[i-1] + xs[i*2-1];
  }
  let total = evens[n/2+1] + odds[n/2];
  let mut mn: u64 = u64::max_value();
  for i in 0..(n/2+1) {
    mn = min(mn, evens[i] + odds[n/2] - odds[i]);
  }
  for i in 0..(n/2) {
    mn = min(mn, odds[i] + evens[n/2+1] - evens[i+1]);
  }
  println!("{}", total - mn);
}