use std::io::{self, BufRead};
use std::cmp::max;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (n, m) = (xs[0], xs[1]);
  let mut ixs: Vec<Vec<usize>> = vec![vec![]; n];
  for i in 0..n {
    let k: usize = lines.next().unwrap().unwrap().parse().unwrap();
    for _ in 0..k {
      let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
      ixs[i].push(xs[1]);
    }
  }
  let mut memo: Vec<u64> = vec![0; m];
  let mut nexts: Vec<Vec<usize>> = Vec::new();
  let mut jxs: Vec<usize> = vec![0; n];
  memo[0] = (n as u64).pow(2);
  nexts.push((0..n).map(|i| ixs[i][0]).collect());
  for i in 1..m {
    for j in 0..n {
      if ixs[j][jxs[j]] <= i {
        jxs[j] += 1;
      }
    }
    let mut m: u64 = memo[i-1];
    for j in 0..i {
      let x = memo[j] + (nexts[j].iter().filter(|&&k| k <= i).count() as u64).pow(2);
      m = max(m, x);
    }
    memo[i] = m;
    if memo[i-1] < memo[i] {
      nexts.push((0..n).map(|k| ixs[k][jxs[k]]).collect());
    } else {
      nexts.push(ixs[i-1].clone());
    }
  }
  println!("{}", memo[m-1]);
}