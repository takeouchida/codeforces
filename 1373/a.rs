use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<i64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (a, b, c) = (xs[0], xs[1], xs[2]);
    let x: i64 = if a < c { 1 } else { -1 };
    let y: i64 = if a * b <= c { -1 } else { b };
    println!("{} {}", x, y);
  }
}