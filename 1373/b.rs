use std::io::{self, BufRead};
use std::cmp::min;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let cs: Vec<char> = lines.next().unwrap().unwrap().chars().collect();
    let zeros = cs.iter().filter(|&&c| c == '0').count();
    let ones = cs.len() - zeros;
    let x = min(zeros, ones);
    println!("{}", if x % 2 == 0 { "NET" } else { "DA" });
  }
}