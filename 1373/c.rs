use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let cs: Vec<char> = lines.next().unwrap().unwrap().chars().collect();
    let mut xs: Vec<i32> = vec![0; cs.len()];
    let mut x: i32 = 0;
    for i in 0..cs.len() {
      x += if cs[i] == '+' { 1 } else { -1 };
      xs[i] = x;
    }
    let mut k: i32 = -1;
    let mut ans: i64 = cs.len() as i64;
    for i in 0..cs.len() {
      if xs[i] == k {
        ans += i as i64 + 1;
        k -= 1;
      }
    }
    println!("{}", ans);
  }
}