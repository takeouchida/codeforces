use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let mut even_sums: Vec<usize> = vec![0; (n + 3) / 2];
    let mut odd_sums: Vec<usize> = vec![0; (n + 2) / 2];
    let mut memo1: Vec<usize> = vec![0; (n + 2) / 2];
    let mut memo2: Vec<usize> = vec![0; (n + 3) / 2];
    for i in 0..n {
      if i % 2 == 0 {
        even_sums[i/2+1] = even_sums[i/2] + xs[i];
      } else {
        odd_sums[i/2+1] = odd_sums[i/2] + xs[i];
      }
    }
    for i in 1..n {
      if i % 2 == 0 {
      } else {
      }
    }
  }
}