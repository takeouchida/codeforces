use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (x, y, n) = (xs[0], xs[1], xs[2]);
    let m = n % x;
    let ans = if y <= m { n - (m - y) } else { n + y - m - x };
    println!("{}", ans);
  }
}