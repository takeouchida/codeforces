use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let mut n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let mut n2: usize = 0;
    let mut n3: usize = 0;
    while n % 2 == 0 {
      n2 += 1;
      n /= 2;
    }
    while n % 3 == 0 {
      n3 += 1;
      n /= 3;
    }
    if n > 1 {
      println!("-1");
    } else if n2 > n3 {
      println!("-1");
    } else {
      println!("{}", 2 * n3 - n2);
    }
  }
}