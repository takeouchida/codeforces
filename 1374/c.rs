use std::io::{self, BufRead};
use std::cmp::min;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let _ = lines.next().unwrap().unwrap();
    let cs: Vec<char> = lines.next().unwrap().unwrap().chars().collect();
    let mut x: i32 = 0;
    let mut m: i32 = 0;
    for c in cs {
      x += if c == ')' { -1 } else { 1 };
      m = min(m, x);
    }
    println!("{}", -m);
  }
}