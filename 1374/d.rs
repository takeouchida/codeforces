use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (n, k) = (xs[0] as usize, xs[1]);
    let mut xs: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    for i in 0..n {
      xs[i] = (k - (xs[i] % k)) % k;
    }
    xs.sort();
    let mut m: u64 = xs[0];
    let mut count: u64 = if xs[0] == 0 { 0 } else { 1 };
    let mut max_m: u64 = 0;
    let mut max_count: u64 = 0;
    for i in 1..n {
      if xs[i] == 0 {
        continue;
      }
      if xs[i] != m {
        if max_count <= count {
          max_count = count;
          max_m = m;
        }
        count = 1;
        m = xs[i]
      } else {
        count += 1;
      }
    }
    if max_count <= count {
      max_count = count;
      max_m = m;
    }
    let ans = if max_count == 0 { 0 } else { 1 + (max_count - 1) * k + max_m };
    println!("{}", ans);
  }
}