use std::io::{self, BufRead};
use std::cmp::min;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let (n, k) = (xs[0], xs[1]);
  let mut ys: Vec<u64> = Vec::new();
  let mut zs: Vec<u64> = Vec::new();
  let mut ws: Vec<u64> = Vec::new();
  let mut vs: Vec<u64> = Vec::new();
  for _ in 0..n {
    let xs: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (t, a, b) = (xs[0], xs[1], xs[2]);
    if a == 1 && b == 1 {
      ws.push(t);
    } else if a == 1 {
      ys.push(t);
    } else if b == 1 {
      zs.push(t);
    }
  }

  if ys.len() + ws.len() < k || zs.len() + ws.len() < k {
    println!("-1");
    return;
  }

  ws.sort();
  if k <= ws.len() {
    let ans: u64 = ws.iter().take(k).sum();
    println!("{}", ans);
    return;
  }

  ys.sort();
  zs.sort();
  for i in 0..min(ys.len(), zs.len()) {
    vs.push(ys[i] + zs[i]);
  }
  let mut ss: Vec<u64> = vec![0; ws.len() + 1];
  let mut ts: Vec<u64> = vec![0; vs.len() + 1];
  for i in 0..ws.len() {
    ss[i+1] = ss[i] + ws[i];
  }
  for i in 0..vs.len() {
    ts[i+1] = ts[i] + vs[i];
  }
  let mut ans: u64 = u64::max_value();
  for i in 0..ss.len() {
    let j = ss.len() - i - 1;
    let l = k - j;
    if ts.len() <= l {
      break;
    }
    ans = min(ans, ss[j] + ts[l]);
  }
  println!("{}", ans);
}