use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let mut xs: Vec<i32> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    for i in 0..n {
      if (i % 2 == 0 && xs[i] < 0) || (i % 2 == 1 && xs[i] > 0) {
        xs[i] *= -1;
      }
    }
    for i in 0..n {
      print!("{} ", xs[i]);
    }
    println!();
  }
}