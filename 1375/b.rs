use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (n, m) = (xs[0], xs[1]);
    let mut ok = true;
    for i in 0..n {
      let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
      if i == 0 || i == n - 1 {
        for j in 0..m {
          if j == 0 || j == m - 1 {
            if xs[j] > 2 {
              ok = false;
            }
          } else {
            if xs[j] > 3 {
              ok = false;
            }
          }
        }
      } else {
        for j in 0..m {
          if j == 0 || j == m - 1 {
            if xs[j] > 3 {
              ok = false;
            }
          } else {
            if xs[j] > 4 {
              ok = false;
            }
          }
        }
      }
    }
    println!("{}", if ok { "YES" } else { "NO" });
    if ok {
      for i in 0..n {
        print!("{}", if i == 0 || i == n - 1 { 2 } else { 3 });
        for _ in 1..(m-1) {
          print!(" {}", if i == 0 || i == n - 1 { 3 } else { 4 });
        }
        println!(" {}", if i == 0 || i == n - 1 { 2 } else { 3 });
      }
    }
  }
}