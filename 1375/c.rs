use std::io::{self, BufRead};
use std::cmp::max;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _  in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let mut ms: Vec<usize> = vec![0; n];
    ms[n-1] = xs[n-1];
    for i in (0..(n-1)).rev() {
      ms[i] = max(xs[i], ms[i+1]);
    }
    let mut x = xs[0];
    let mut ok = true;
    for i in 1..n {
      if xs[i-1] >= xs[i] && x >= ms[i] {
        ok = false;
        x = xs[i];
      }
    }
    println!("{}", if ok { "YES" } else { "NO" });
  }
}