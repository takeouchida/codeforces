use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let mut xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let mut ans: Vec<usize> = Vec::new();
    for _ in 0..(2*n) {
      let mut bs: Vec<bool> = vec![false; n + 1];
      let mut sorted = true;
      bs[xs[0]] = true;
      for i in 1..n {
        bs[xs[i]] = true;
        sorted = sorted && xs[i-1] <= xs[i];
      }
      let k = bs.iter().position(|b| !b).unwrap();
      if k == n {
        if sorted {
          break;
        } else {
          for i in 0..n {
            if xs[i] != i {
              xs[i] = n;
              ans.push(i + 1);
              break;
            }
          }
        }
      } else {
        for i in k..n {
          if xs[i] != i {
            xs[i] = k;
            ans.push(i + 1);
            break;
          }
        }
      }
    }
    println!("{}", ans.len());
    for i in 0..ans.len() {
      print!("{} ", ans[i]);
    }
    println!();
  }
}