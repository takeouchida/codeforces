use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let mut ys = xs.clone();
  let mut zs = xs.clone();
  zs.sort();
  let mut ans: Vec<(usize, usize)> = Vec::new();
  for j in (1..n).rev() {
    if zs[j] == ys[j] {
      continue;
    }
    for i in 0..j {
      if ys[i] == zs[j] && xs[i] > xs[j] {
        ans.push((i + 1, j + 1));
        let temp = ys[i];
        ys[i] = ys[j];
        ys[j] = temp;
        break;
      }
    }
  }
  println!("{:?}", ys);
  println!("{}", ans.len());
  for i in 0..ans.len() {
    let (x, y) = ans[i];
    println!("{} {}", x, y);
  }
}