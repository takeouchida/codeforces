use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let s: Vec<char> = "abacaba".chars().collect();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let cs: Vec<char> = lines.next().unwrap().unwrap().chars().collect();
    let ixs: Vec<usize> = (0..=(n-7)).filter(|i| (0..7).all(|j| cs[i+j] == s[j])).collect();
    if ixs.len() == 1 {
      let mut ans = String::new();
      let k = ixs[0];
      for i in 0..k {
        ans.push(if cs[i] == '?' { 'z' } else { cs[i] });
      }
      for i in 0..7 {
        ans.push(s[i]);
      }
      for i in (k+7)..n {
        ans.push(if cs[i] == '?' { 'z' } else { cs[i] });
      }
      println!("Yes\n{}", ans);
    } else if ixs.len() > 1 {
      println!("No");
    } else {
      let ixs: Vec<usize> = (0..=(n-7)).filter(|i| (0..7).all(|j| cs[i+j] == s[j] || cs[i+j] == '?' )).collect();
      if ixs.len() == 0 {
        println!("No");
      } else {
        let k = ixs[0];
        let mut ans = String::new();
        for i in 0..k {
          ans.push(if cs[i] == '?' { 'z' } else { cs[i] });
        }
        for i in 0..7 {
          ans.push(s[i]);
        }
        for i in (k+7)..n {
          ans.push(if cs[i] == '?' { 'z' } else { cs[i] });
        }
        println!("Yes\n{}", ans);
      }
    }
  }
}