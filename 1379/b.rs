use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<i64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (l, r, m) = (xs[0], xs[1], xs[2]);
    let d = r - l;
    let p = m + r - l;
    let n = p / l;
    let s = p % l;
    let (a, b, c) = if s < d {
      (0, 0, s)
    } else if s < 2 * d {
      (0, d, s - d)
    } else {
      let t = (s - d) / n;
      (t, d, s - d - t * n)
    };
    println!("{} {} {}", a + l, b + l, r - c);
  }
}