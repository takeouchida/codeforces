use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (n, m) = (xs[0], xs[1]);
    let mut a: Vec<(usize, u64)> = Vec::new();
    let mut b: Vec<(usize, u64)> = Vec::new();
    for i in 0..m {
      let xs: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
      a.push((i, xs[0]));
      b.push((i, xs[1]));
    }
    lines.next();
    a.sort_by(|x, y| y.1.cmp(&x.1));
    let (bi, b_max) = b.iter().max_by(|x, y| x.1.cmp(&y.1)).unwrap().clone();
    let num_a = a.iter().filter(|&x| x.1 >= b_max).count();
    if n <= num_a {
      let ans: u64 = a[0..n].iter().map(|x| x.1).sum();
      println!("{}", ans);
    } else {
      let sum_a: u64 = a[0..num_a].iter().map(|x| x.1).sum();
      let mut c: Vec<u64> = a.iter().filter(|&x| x.1 >= b_max).map(|&x| b[x.0].1).collect();
      c.sort_by(|x, y| y.cmp(&x));
      println!("{:?}", c);
    }
  }
}