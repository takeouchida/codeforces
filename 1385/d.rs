use std::io::{self, BufRead};
use std::cmp::min;

fn find_min(counts: &Vec<Vec<usize>>, lo: usize, hi: usize, c: usize) -> usize {
  if lo + 1 == hi {
    1 - (counts[c][hi] - counts[c][lo])
  } else {
    let mid = (lo + hi) / 2;
    let n = mid - lo;
    min(n - (counts[c][mid] - counts[c][lo]) + find_min(counts, mid, hi, c + 1),
        n - (counts[c][hi] - counts[c][mid]) + find_min(counts, lo, mid, c + 1))
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let cs: Vec<usize> = lines.next().unwrap().unwrap().chars().map(|c| c as usize - 'a' as usize).collect();
    let mut counts: Vec<Vec<usize>> = vec![vec![0; n + 1]; 26];
    for i in 0..26 {
      for j in 1..=n {
        counts[i][j] = counts[i][j-1] + (if cs[j-1] == i { 1 } else { 0 });
      }
    }
    let ans = find_min(&counts, 0, n, 0);
    println!("{}", ans);
  }
}