use std::io::{self, BufRead};
use std::collections::{BTreeMap, BTreeSet};
use std::cmp::min;

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
enum Dir { UNDIR, RIGHT, LEFT }

fn dfs(es: &Vec<Vec<usize>>, visited: &mut BTreeMap<usize, usize>, untouched: &mut BTreeSet<usize>, ds: &mut BTreeMap<(usize, usize), Dir>, u: usize, i: usize) -> Option<usize> {
  if visited.contains_key(&u) {
    return visited.get(&u).cloned();
  }
  visited.insert(u, i);
  untouched.remove(&u);
  let mut ret = None;
  for &v in &es[u] {
    if i > 0 && visited.get(&v) == Some(&(i - 1)) {
      continue;
    }
    let (u_, v_) = if u < v { (u, v) } else { (v, u) };
    let d = ds.get(&(u_, v_));
    let changed = d == Some(&Dir::UNDIR);
    match (d, u < v) {
      (Some(Dir::UNDIR), true ) => { ds.insert((u_, v_), Dir::RIGHT); },
      (Some(Dir::UNDIR), false) => { ds.insert((u_, v_), Dir::LEFT);  },
      (Some(Dir::LEFT ), true ) => { continue; },
      (Some(Dir::RIGHT), false) => { continue; },
      _ => (),
    };
    let res = dfs(es, visited, untouched, ds, v, i + 1);
    println!("{} {} {:?}", u, v, res);
    if res.is_some() && changed {
      ds.insert((u_, v_), Dir::UNDIR);
    }
    ret = match (ret, res) {
      (Some(j), Some(k)) => Some(min(j, k)),
      _                  => None,
    };
  }
  visited.remove(&u);
  match ret {
    None    => None,
    Some(j) => if j < i { Some(j) } else { None },
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let (n, m) = (xs[0], xs[1]);
    let mut ess: Vec<Vec<usize>> = vec![vec![]; n];
    let mut ds: BTreeMap<(usize, usize), Dir> = BTreeMap::new();
    let mut es: Vec<(usize, usize)> = Vec::new();
    for _ in 0..m {
      let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
      let (d, u, v) = (xs[0], xs[1] - 1, xs[2] - 1);
      es.push((u, v));
      ess[u].push(v);
      if d == 0 {
        ess[v].push(u);
        let (u, v) = if u < v { (u, v) } else { (v, u) };
        ds.insert((u, v), Dir::UNDIR);
      }
    }
    let mut visited: BTreeMap<usize, usize> = BTreeMap::new();
    let mut untouched: BTreeSet<usize> = (0..n).collect();
    while !untouched.is_empty() {
      let &v = untouched.iter().next().unwrap();
      dfs(&ess, &mut visited, &mut untouched, &mut ds, v, v);
    }
    let ok = ds.values().all(|&d| d != Dir::UNDIR);
    println!("{}", if ok { "YES" } else { "NO" });
    if ok {
      for (u, v) in es {
        let (u_, v_) = if u < v { (u, v) } else { (v, u) };
        let d = ds.get(&(u_, v_));
        match (d, u < v) {
          (Some(Dir::RIGHT), false) => { println!("{} {}", v + 1, u + 1); },
          (Some(Dir::LEFT), true) => { println!("{} {}", v + 1, u + 1); },
          _ => { println!("{} {}", u + 1, v + 1); },
        }
      }
    }
  }
}