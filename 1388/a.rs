use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    if n <= 30 {
      println!("NO");
    } else {
      println!("YES");
      match n {
        36 => { println!("1 6 14 15"); },
        40 => { println!("1 10 14 15"); },
        44 => { println!("5 10 14 15"); },
        _ => { println!("14 10 6 {}", n - 30); },
      }
    }
  }
}