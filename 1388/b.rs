use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let d: usize = (n + 3) / 4;
    let e: usize = n - d;
    let mut s = String::new();
    for _ in 0..e {
      s.push('9');
    }
    for _ in 0..d {
      s.push('8');
    }
    println!("{}", s);
  }
}