use std::io::{self, BufRead};
use std::collections::BTreeSet;

fn dfs(u: usize, es: &Vec<Vec<usize>>, visited: &mut BTreeSet<usize>, ps: &Vec<i32>, hs: &Vec<i32>) -> Option<(i32, i32)> {
  visited.insert(u);
  let mut sum_h: i32 = 0;
  let mut sum_p: i32 = ps[u];
  for &v in &es[u] {
    if !visited.contains(&v) {
      let o = dfs(v, es, visited, ps, hs);
      match o {
        None => { return None; },
        Some((p, h)) => {
          sum_h += h;
          sum_p += p;
        },
      }
    }
  }
  if -sum_p <= hs[u] && hs[u] <= sum_p {
    let d = sum_p - hs[u];
    let h = sum_p - d / 2;
    if d % 2 != 0 || sum_h > h {
      None
    } else {
      Some((sum_p, h))
    }
  } else {
    None
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let n = xs[0];
    let ps: Vec<i32> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let hs: Vec<i32> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let mut es: Vec<Vec<usize>> = vec![vec![]; n];
    for _ in 0..(n-1) {
      let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
      let (u, v) = (xs[0] - 1, xs[1] - 1);
      es[u].push(v);
      es[v].push(u);
    }
    let mut visited = BTreeSet::new();
    let res = dfs(0, &es, &mut visited, &ps, &hs);
    println!("{}", match res {
      None => "NO",
      Some(_) => "YES",
    });
  }
}