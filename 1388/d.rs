use std::io::{self, BufRead};
use std::collections::BTreeSet;

fn dfs(u: usize, es: &Vec<Vec<usize>>, xs: &Vec<i32>, memo: &mut Vec<Option<i32>>, order: &mut Vec<usize>, ans: &mut i32) -> i32 {
  match memo[u] {
    None => {
      let mut sum: i32 = xs[u];
      for &v in &es[u] {
        let p = dfs(v, es, xs, memo, order, ans);
        if p > 0 {
          sum += p;
        }
      }
      memo[u] = Some(sum);
      if sum > 0 {
        *ans += sum;
        order.push(u);
      }
      sum
    },
    Some(p) => p,
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let xs: Vec<i32> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let ys: Vec<i32> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let mut es: Vec<Vec<usize>> = vec![vec![]; n];
  for u in 0..n {
    let v = ys[u];
    if v > 0 {
      es[v as usize - 1].push(u);
    }
  }
  let mut memo: Vec<Option<i32>> = vec![None; n];
  let mut ans: i32 = 0;
  let mut order: Vec<usize> = Vec::new();
  for u in 0..n {
    match memo[u] {
      None => {
        dfs(u, &es, &xs, &mut memo, &mut order, &mut ans);
      },
      Some(_) => (),
    }
  }
  let visited: BTreeSet<_> = order.iter().cloned().collect();
  for u in 0..n {
    if !visited.contains(&u) {
      order.push(u);
      if let Some(p) = memo[u] {
        ans += p;
      }
    }
  }
  println!("{}", ans);
  for i in 0..n {
    print!("{} ", order[i] + 1);
  }
  println!();
}