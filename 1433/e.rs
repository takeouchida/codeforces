use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: u64 = lines.next().unwrap().unwrap().parse().unwrap();
  let total = match n {
    2 => 1,
    4 => 3,
    _ => ((n/2+1)..n).product::<u64>() * (2..n/2).product::<u64>(),
  };
  println!("{}", total);
}