use std::io::{self, BufRead};

const M: u64 = 998244353;

fn pow(n: u64, p: u64) -> u64 {
  let mut q = p;
  let mut r = n;
  let mut result = 1;
  while q > 0 { 
    if q % 2 == 1 { 
      result = result * r % M;
    }   
    r = r * r % M;
    q >>= 1;
  }
  result
}

fn inv(n: u64) -> u64 {
  pow(n, M - 2)
}

fn factorial(n: u64) -> u64 {
  (1..=n).fold(1, |acc, x| acc * x % M)
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let mut xs: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  xs.sort();
  let mut ys: Vec<u64> = vec![0; n * 2 + 1];
  for i in 0..(n * 2) {
    ys[i + 1] = ys[i] + xs[i]
  }
  let num = factorial((n as u64 - 1) * 2);
  let denom = inv(pow(factorial(n as u64 - 1), 2) % M);
  let factor = num * denom % M;
  println!("f {}", factor);
  let mut sum: u64 = 0;
  println!("xs {:?}", xs);
  println!("ys {:?}", ys);
  for i in 1..(n * 2) {
    let z = ((xs[i] * i as u64 - ys[i]) % M) * 2;
    println!("z {}", z);
    sum = (z + sum) % M;
  }
  sum = sum * factor % M;
  println!("{}", sum);
}