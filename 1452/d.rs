use std::io::{self, BufRead};

const M: u64 = 998244353;

fn pow(n: u64, p: u64) -> u64 {
  let mut q = p;
  let mut r = n;
  let mut result = 1;
  while q > 0 { 
    if q % 2 == 1 { 
      result = result * r % M;
    }   
    r = r * r % M;
    q >>= 1;
  }
  result
}

fn inv(n: u64) -> u64 {
  pow(n, M - 2)
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let mut cs: Vec<u64> = vec![0, 0];
  let mut x: u64 = 0;
  for i in 0..n {
    let p = i % 2;
    let q = (i + 1) % 2;
    x = (cs[q] + q as u64) % M;
    cs[p] = (cs[p] + x) % M;
  }
  let d = pow(2, n as u64);
  println!("{}", x * inv(d) % M);
}