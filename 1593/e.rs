use std::io::{self, BufRead};
use std::collections::BTreeSet;

#[derive(Debug)]
struct State {
  vis: BTreeSet<usize>,
  ps: Vec<u32>,
}

impl State {
  fn new(n: usize) -> State {
    State { vis: BTreeSet::new(), ps: vec![u32::MAX; n] }
  }

  fn dfs(&mut self, u: usize, e: &Vec<Vec<usize>>) {
    self.vis.insert(u);
    let mut ps: Vec<u32> = vec![];
    for v in &e[u] {
      if self.vis.contains(&v) {
        continue;
      }
      self.dfs(*v, e);
      ps.push(self.ps[*v]);
    }
    if e[u].len() <= 1 {
      self.ps[u] = 0;
    } else {
      let l = e[u].len();
      let mut ps: Vec<u32> = e[u].iter().map(|v| self.ps[*v]).collect();
      ps.sort();
      if ps[l - 2] < u32::MAX {
        self.ps[u] = ps[l - 2] + 1;
      }
    }
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    lines.next();
    let x: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let n = x[0];
    let k = x[1] as u32;
    let mut e: Vec<Vec<usize>> = vec![vec![]; n];
    for _ in 0..(n - 1) {
      let x: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
      let u = x[0] - 1;
      let v = x[1] - 1;
      e[u].push(v);
      e[v].push(u);
    }
    let mut st = State::new(n);
    st.dfs(0, &e);
    st.vis.clear();
    st.dfs(0, &e);
    println!("{}", st.ps.iter().filter(|&&u| k <= u).count());
  }
}