use std::io::{self, BufRead};
use std::collections::BTreeSet;
use std::cmp::max;

#[derive(PartialEq, Clone, Copy)]
enum Role { Unknown, Imposter, Crewmate }

fn dfs(u: usize, dp: &mut Vec<Vec<usize>>, e: &Vec<Vec<(usize, bool)>>, vis: &mut BTreeSet<usize>, roots: &mut BTreeSet<usize>, is_root: bool) {
  vis.insert(u);
  let mut n0 = 1;
  let mut n1 = 0;
  for (v, is_crewmate) in &e[u] {
    if vis.contains(&v) {
      continue;
    }
    dfs(*v, dp, e, vis, roots, false);
    if *is_crewmate {
      n1 += dp[*v][1]; // u: crewmate, v: crewmate
      n0 += dp[*v][0]; // u: imposter, v: imposter
    } else {
      n1 += dp[*v][0]; // u: crewmate, v: imposter
      n0 += dp[*v][1]; // u: imposter, v: crewmate
    }
  }
  dp[u][0] = n0;
  dp[u][1] = n1;
  if is_root {
    roots.insert(u);
  } else {
    roots.remove(&u);
  }
}

fn check(u: usize, roles: &mut Vec<Role>, e: &Vec<Vec<(usize, bool)>>, vis: &mut BTreeSet<usize>) -> bool {
  vis.insert(u);
  let ru = roles[u];
  if ru == Role::Unknown {
    panic!("Logic error at {}", u);
  }
  for (v, is_crewmate) in &e[u] {
    let rv = roles[*v];
    match (ru, rv, *is_crewmate) {
      (Role::Imposter, Role::Imposter, false) => return false,
      (Role::Imposter, Role::Crewmate, true) => return false,
      (Role::Crewmate, Role::Imposter, true) => return false,
      (Role::Crewmate, Role::Crewmate, false) => return false,
      (Role::Imposter, Role::Unknown, false) => roles[*v] = Role::Crewmate,
      (Role::Imposter, Role::Unknown, true) => roles[*v] = Role::Imposter,
      (Role::Crewmate, Role::Unknown, true) => roles[*v] = Role::Crewmate,
      (Role::Crewmate, Role::Unknown, false) => roles[*v] = Role::Imposter,
      _ => (),
    }
    if rv == Role::Unknown {
      let b = check(*v, roles, e, vis);
      if !b {
        return false;
      }
    }
  }
  true
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let x: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let n = x[0];
    let m = x[1];
    let mut dp: Vec<Vec<usize>> = vec![vec![0; 2]; n];
    let mut e: Vec<Vec<(usize, bool)>> = vec![vec![]; n];
    let mut roots: BTreeSet<usize> = BTreeSet::new();
    for _ in 0..m {
      let l: String = lines.next().unwrap().unwrap();
      let x: Vec<&str> = l.split(' ').collect();
      let i = x[0].parse::<usize>().unwrap() - 1;
      let j = x[1].parse::<usize>().unwrap() - 1;
      let c = x[2] == "crewmate";
      e[i].push((j, c));
    }

    // detect contradiction
    let mut vis = BTreeSet::<usize>::new();
    let mut contra = false;
    for i in 0..n {
      if vis.contains(&i) {
        continue;
      }
      let mut roles: Vec<Role> = vec![Role::Unknown; n];
      roles[i] = Role::Imposter;
      let b0 = check(i, &mut roles, &e, &mut vis);
      if !b0 {
        contra = true;
      }
      let mut roles: Vec<Role> = vec![Role::Unknown; n];
      roles[i] = Role::Crewmate;
      let b1 = check(i, &mut roles, &e, &mut vis);
      if !b1 {
        contra = true;
      }
    }
    if contra {
      println!("-1");
      continue;
    }

    // count
    for i in 0..n {
      let mut vis = BTreeSet::<usize>::new();
      if dp[i][0] > 0 {
        continue;
      }
      dfs(i, &mut dp, &e, &mut vis, &mut roots, true);
    }
    let count = roots.iter().map(|u| max(dp[*u][0], dp[*u][1])).sum::<usize>();
    println!("{}", count);
  }
}