use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let mut a: Vec<u64> = vec![0; n];
    let mut b: Vec<u64> = vec![0; n];
    let mut p: Vec<(usize, usize)> = vec![];
    for _ in 0..n {
      let x: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
      let i = x[0] - 1;
      let j = x[1] - 1;
      a[i] += 1;
      b[j] += 1;
      p.push((i, j));
    }
    let mut sub = 0;
    for (x, y) in p {
      sub += (a[x] - 1) * (b[y] - 1);
    }
    let ans = n as u64 * (n as u64 - 1) * (n as u64 - 2) / 6 - sub;
    println!("{}", ans);
  }
}