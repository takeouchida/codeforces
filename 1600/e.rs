use std::io::{self, BufRead};

enum Result {
  Head, Tail, Lose
}

fn decide(a: &[i64], p: usize, q: usize, last: i64) -> Result {
  if q <= p {
    return Result::Lose;
  }
  if last < a[p] {
    if last < a[q - 1] {
      if a[p] < a[q - 1] {
        return Result::Head;
      } else {
        return Result::Tail;
      }
    } else {
      return Result::Head;
    }
  } else {
    if last < a[q - 1] {
      return Result::Tail;
    } else {
      return Result::Lose;
    }
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let a: Vec<i64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let mut p: usize = 0;
  let mut q: usize = n;
  let mut last: i64 = -1;
  loop {
    match decide(&a, p, q, last) {
      Result::Head => {
        last = a[p];
        p += 1;
      },
      Result::Tail => {
        last = a[q - 1];
        q -= 1;
      },
      Result::Lose => {
        println!("Bob");
        break;
      }
    }
    match decide(&a, p, q, last) {
      Result::Head => {
        last = a[p];
        p += 1;
      },
      Result::Tail => {
        last = a[q - 1];
        q -= 1;
      },
      Result::Lose => {
        println!("Alice");
        break;
      }
    }
  }
}