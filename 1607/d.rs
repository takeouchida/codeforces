use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..n {
    let count: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let xs: Vec<i32> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let cs: Vec<char> = lines.next().unwrap().unwrap().chars().collect();
    let mut blues: Vec<i32> = vec![];
    let mut reds: Vec<i32> = vec![];
    for i in 0..count {
      if cs[i] == 'B' {
        blues.push(xs[i]);
      } else {
        reds.push(xs[i]);
      }
    }
    blues.sort();
    reds.sort();
    let answer = (0..blues.len()).all(|i| (i as i32) + 1 <= blues[i]) && (0..reds.len()).all(|i| reds[i] <= (i + blues.len() + 1) as i32);
    println!("{}", if answer { "YES" } else { "NO" });
  }
}