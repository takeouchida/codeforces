use std::io::{self, BufRead};
use std::cmp::{min, max};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let z: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let height = z[0] as i32;
    let width = z[1] as i32;
    let cs: Vec<char> = lines.next().unwrap().unwrap().chars().collect();
    let mut regions: Vec<(i32, i32, i32, i32)> = vec![(0, 0, 0, 0); cs.len() + 1];
    let mut x: i32 = 0;
    let mut y: i32 = 0;
    for i in 0..cs.len() {
      match cs[i] {
        'L' => x -= 1,
        'R' => x += 1,
        'U' => y += 1,
        'D' => y -= 1,
        _ => panic!("invalid input {}", cs[i]),
      }
      regions[i + 1] = (min(regions[i].0, x), min(regions[i].1, y), max(regions[i].2, x), max(regions[i].3, y));
    }
    for i in (0..regions.len()).rev() {
      let (x1, y1, x2, y2) = regions[i];
      if x2 - x1 < width && y2 - y1 < height {
        let c = - x1 + 1;
        let r = y2 + 1;
        println!("{} {}", r, c);
        break;
      }
    }
  }
}