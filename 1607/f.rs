use std::io::{self, BufRead};
use std::collections::BTreeSet;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    lines.next();
    let x: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let height = x[0] as i32;
    let width = x[1] as i32;
    let mut board: Vec<Vec<char>> = vec![];
    for _ in 0..height {
      let cs: Vec<char> = lines.next().unwrap().unwrap().chars().collect();
      board.push(cs);
    }
    let mut counts: Vec<Vec<usize>> = vec![vec![0; width as usize]; height as usize];
    for i in 0..height {
      for j in 0..width {
        if counts[i as usize][j as usize] != 0 {
          continue;
        }
        let mut path: Vec<(i32, i32)> = vec![];
        let mut visited: BTreeSet<(i32, i32)> = BTreeSet::new();
        let mut y = i;
        let mut x = j;
        loop {
          let l = path.len();
          if x < 0 || width <= x || y < 0 || height <= y {
            for k in 0..l {
              let (y_, x_) = path[l - k - 1];
              counts[y_ as usize][x_ as usize] = k + 1;
            }
            break;
          }
          if visited.contains(&(y, x)) {
            let c = 1 + path.iter().rev().position(|(y_, x_)| *x_ == x && *y_ == y).unwrap();
            for k in 0..c {
              let (y_, x_) = path[l - k - 1];
              counts[y_ as usize][x_ as usize] = c;
            }
            for k in 0..(l - c) {
              let (y_, x_) = path[l - c - k - 1];
              counts[y_ as usize][x_ as usize] = c + k + 1;
            }
            break;
          }
          let c = counts[y as usize][x as usize];
          if c != 0 {
            for k in 0..l {
              let (y_, x_) = path[l - k - 1];
              counts[y_ as usize][x_ as usize] = c + k + 1;
            }
            break;
          }
          path.push((y, x));
          visited.insert((y, x));
          match board[y as usize][x as usize] {
            'L' => x -= 1,
            'R' => x += 1,
            'U' => y -= 1,
            'D' => y += 1,
            _ => panic!("Invalid input {}", board[y as usize][x as usize]),
          }
        }
      }
    }
    let mut x = 0;
    let mut y = 0;
    let mut max_c = 0;
    for i in 0..height {
      for j in 0..width {
        let c = counts[i as usize][j as usize];
        if max_c < c {
          max_c = c;
          x = j + 1;
          y = i + 1;
        }
      }
    }
    println!("{} {} {}", y, x, max_c);
  }
}