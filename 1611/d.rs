use std::io::{self, BufRead};
use std::iter::Iterator;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let b: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse::<usize>().unwrap() - 1).collect();
    let p: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse::<usize>().unwrap() - 1).collect();
    let root = b.iter().enumerate().find(|&(i, &x)| i == x).unwrap().0;
    let mut d: Vec<Option<usize>> = vec![None; n];
    if root != p[0] {
      println!("-1");
      continue;
    }
    d[root] = Some(0);
    let mut is_valid = true;
    for i in 1..n {
      let u = p[i];
      let par = b[u];
      if let Some(_) = d[par] {
        d[u] = Some(i);
      } else {
        is_valid = false;
        break;
      }
    }
    if is_valid {
      for i in 0..n {
        let w = d[i].unwrap() - d[b[i]].unwrap();
        print!("{} ", w);
      }
      println!();
    } else {
      println!("-1");
    }
  }
}