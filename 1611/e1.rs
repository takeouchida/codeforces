use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    lines.next();
    let a: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let n = a[0];
    let _k = a[1];
    let _x: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let mut e: Vec<Vec<usize>> = vec![vec![]; n];
    for _ in 0..(n - 1) {
      let a: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
      let u = a[0] - 1;
      let v = a[1] - 1;
      e[u].push(v);
      e[v].push(u);
    }
  }
}