use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let x: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let n = x[0];
    let mut s = x[1] as i64;
    let a: Vec<i64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let mut i: usize = 0;
    let mut j: usize = 0;
    let mut ans: i64 = -1;
    let mut max_i: usize = 0;
    let mut max_j: usize = 0;
    while i < n {
      if i < j && (j == n || i == j || s < 0) {
        s -= a[i];
        i += 1;
      } else {
        s += a[j];
        j += 1;
      }
      let k = (j - i) as i64;
      if i < j && s >= 0 && ans < k {
        max_i = i;
        max_j = j;
        ans = k as i64;
      }
    }
    if ans >= 0 {
      println!("{} {}", max_i + 1, max_j);
    } else {
      println!("-1");
    }
  }
}