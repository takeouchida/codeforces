use std::io::{self, BufRead};

const M: u64 = 998244353;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let _: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let a: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let dp_len = *a.iter().max().unwrap() as usize + 2;
    let mut dp: Vec<Vec<u64>> = vec![vec![0; 2]; dp_len];
    for i in 0..a.len() {
      let j = a[i] as usize;
      dp[j + 1][0] += dp[j][0] + dp[j + 1][0] + if j == 0 { 1 } else { 0 };
      if j >= 2 {
        dp[j - 1][1] += dp[j - 1][0] + dp[j - 1][1];
      } else if j == 1 {
        dp[0][1] += dp[0][1] + 1;
      }
    }
    let mut ans = 0;
    for i in 0..dp_len {
      ans = ((ans + dp[i][0]) % M + dp[i][1]) % M;
    }
    println!("{:?}", dp);
    println!("{}", ans);
  }
}