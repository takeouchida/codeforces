use std::io::{self, BufRead};
use std::cmp::min;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let x: Vec<i64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    if n == 1 {
      println!("0");
      continue;
    }
    let mut ans: usize = n;
    for i in 0..(n - 1) {
      for j in (i + 1)..n {
        let nu: i64 = x[j] - x[i];
        let d: i64 = (j - i) as i64;
        let b: i64 = x[i] * d - (nu * i as i64);
        let cnt = (0..n).filter(|&i| x[i as usize] * d != nu * i as i64 + b).count();
        ans = min(ans, cnt);
      }
    }
    println!("{}", ans);
  }
}