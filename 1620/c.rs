use std::io::{self, BufRead};
use std::iter;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let mut k = xs[1];
    let mut x = xs[2] - 1;
    let cs: Vec<char> = lines.next().unwrap().unwrap().chars().collect();
    let mut is_ast = true;
    let mut xs: Vec<usize> = vec![]; // 'a'
    let mut ys: Vec<usize> = vec![]; // '*'
    let mut v: usize = 0;
    for c in cs {
      if c == '*' {
        if is_ast {
          v += 1;
        } else {
          xs.push(v);
          is_ast = true;
          v = 1;
        }
      } else {
        if is_ast {
          ys.push(v);
          is_ast = false;
          v = 1;
        } else {
          v += 1;
        }
      }
    }
    if is_ast {
      ys.push(v);
    } else {
      xs.push(v);
      ys.push(0);
    }
    let mut units: Vec<usize> = vec![];
    let mut v: usize = 1;
    for y in ys.iter().rev() {
      if *y == 0 {
        units.push(0);
      } else {
        units.push(v);
        v *= k * y + 1;
      }
    }
    units = units.into_iter().rev().collect();
    for i in 0..units.len() {
      if i > 0 {
        print!("{}", iter::repeat("a").take(xs[i - 1]).collect::<String>());
      }
      let u = units[i];
      if u != 0 {
        let c = x / u;
        x = x % u;
        print!("{}", iter::repeat("b").take(c).collect::<String>());
      }
    }
    println!();
  }
}