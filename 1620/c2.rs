use std::io::{self, BufRead};
use std::iter;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let k = xs[1];
    let mut x = xs[2] - 1;
    let cs: Vec<char> = lines.next().unwrap().unwrap().chars().collect();
    let mut is_ast = true;
    let mut xs: Vec<usize> = vec![]; // 'a'
    let mut ys: Vec<usize> = vec![]; // '*'
    let mut v: usize = 0;
    for c in cs {
      if c == '*' {
        if is_ast {
          v += 1;
        } else {
          xs.push(v);
          is_ast = true;
          v = 1;
        }
      } else {
        if is_ast {
          ys.push(v);
          is_ast = false;
          v = 1;
        } else {
          v += 1;
        }
      }
    }
    if is_ast {
      ys.push(v);
    } else {
      xs.push(v);
      ys.push(0);
    }
    let mut zs: Vec<usize> = vec![0; ys.len()];
    for i in (0..ys.len()).rev() {
      if ys[i] == 0 {
        zs[i] = 0;
      } else {
        zs[i] = x % (ys[i] * k + 1);
        x /= ys[i] * k + 1;
      }
    }
    for i in 0..zs.len() {
      if i > 0 {
        print!("{}", iter::repeat("a").take(xs[i - 1]).collect::<String>());
      }
      if zs[i] != 0 {
        print!("{}", iter::repeat("b").take(zs[i]).collect::<String>());
      }
    }
    println!();
  }
}