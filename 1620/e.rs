use std::io::{self, BufRead};
use std::collections::BTreeMap;

fn find_root(parents: &mut Vec<usize>, x: usize) -> usize {
  let r = parents[x];
  if x == r {
    return r;
  }
  let r = find_root(parents, r);
  parents[x] = r;
  r
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let q: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let mut parents: Vec<usize> = Vec::new();
  let mut root_to_elem: BTreeMap<usize, usize> = BTreeMap::new();
  let mut elem_to_root: BTreeMap<usize, usize> = BTreeMap::new();
  for _ in 0..q {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    if xs[0] == 1 {
      let x = xs[1];
      match elem_to_root.get(&x) {
        None => {
          let r = parents.len();
          parents.push(r);
          root_to_elem.insert(r, x);
          elem_to_root.insert(x, r);
        },
        Some(r) => {
          parents.push(*r);
        },
      }
    } else {
      let x = xs[1];
      let y = xs[2];
      if x == y {
        continue;
      }
      if let Some(rx) = elem_to_root.get(&x) {
        match elem_to_root.get(&y) {
          None => {
            root_to_elem.insert(*rx, y);
            elem_to_root.insert(y, *rx);
            elem_to_root.remove(&x);
          },
          Some(ry) => {
            if rx < ry {
              parents[*ry] = *rx;
              root_to_elem.insert(*rx, y);
              elem_to_root.insert(y, *rx);
              elem_to_root.remove(&x);
            } else {
              parents[*rx] = *ry;
              root_to_elem.remove(rx);
              elem_to_root.remove(&x);
            }
          },
        }
      }
    }
  }
  for i in 0..parents.len() {
    let r = find_root(&mut parents, i);
    let e = root_to_elem.get(&r).unwrap();
    print!("{} ", e);
  }
  println!();
}