use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let mut min_l = usize::MAX;
    let mut max_r = usize::MIN;
    let mut l_c = usize::MAX;
    let mut r_c = usize::MAX;
    let mut l_i = 0;
    let mut r_i = 0;
    for i in 0..n {
      let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
      let l = xs[0];
      let r = xs[1];
      let c = xs[2];
      let update_l = (l < min_l || (l == min_l && c < l_c)) && !(l_i == r_i && l == min_l && r < max_r);
      let update_r = (max_r < r || (max_r == r && c < r_c)) && !(l_i == r_i && min_l < l && max_r == r);
      let update_lr_by_cost = l == min_l && max_r == r && ((l_i == r_i && c < l_c) || (l_i != r_i && c < l_c + r_c));
      let update_lr = update_lr_by_cost || (l <= min_l && max_r <= r && (l < min_l || max_r < r));
      if update_lr {
        min_l = l;
        max_r = r;
        l_c = c;
        r_c = c;
        l_i = i;
        r_i = i;
      }
      else {
        if update_l {
          min_l = l;
          l_c = c;
          l_i = i;
        }
        if update_r {
          max_r = r;
          r_c = c;
          r_i = i;
        }
      }
      println!("{}", if l_i == r_i { l_c } else { l_c + r_c });
    }
  }
}