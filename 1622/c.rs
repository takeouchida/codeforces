use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let n = xs[0];
    let k = xs[1];
    let mut a: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let sum = a.iter().sum::<usize>();
    a.sort();

    let bottom = a[0] * n;
    let dig = if k < bottom { (bottom - k - 1) / n + 1 } else { 0 };
    let height = a[0] - dig;

    let mut count = dig;
    let mut rest = sum - dig;
    for i in (1..n).rev() {
      if rest <= k {
        continue;
      }
      rest -= a[i] - height;
      count += 1;
    }
    println!("{} {} {:?}", dig, count, a);

    // let mut i = n - 1;
    // let mut cnt = sum;
    // while 0 < i && k < cnt && a[0] < a[i] {
    //   cnt -= a[i] - a[0];
    //   i -= 1;
    // }
    // let x = n - i - 1;
    // if cnt <= k {
    //   println!("{}", x);
    //   continue;
    // }

    // let dist = cnt - k;
    // let dig = (dist - 1) / n + 1;
    // let h = a[0] - dig;
    // let mut i = n - 1;
    // let mut cnt = sum;
    // while 0 < i && k < cnt {
    //   cnt -= a[i] - h;
    //   i -= 1;
    // }
    // let x = n - i - 1;
    // println!("{}", x + dig);
  }
}