use std::io::{self, BufRead};
use std::collections::BTreeMap;

fn find_substring<'a>(
  t: &'a str,
  css: &'a Vec<String>,
  sub: &BTreeMap<&str, (usize, usize, usize)>,
  memo: &mut BTreeMap<&'a str, Vec<(usize, usize, usize)>>
) -> Vec<(usize, usize, usize)> {
  if let Some(v) = sub.get(t) {
    return vec![*v];
  }
  if let Some(v) = memo.get(t) {
    return v.to_vec();
  }
  if 4 <= t.len() {
    for l in 2..(t.len() - 1) {
      let v1 = find_substring(&t[..l], css, sub, memo);
      let v2 = find_substring(&t[l..], css, sub, memo);
      if !v1.is_empty() && !v2.is_empty() {
        let mut s = v1.clone();
        let mut s2 = v2.clone();
        s.append(&mut s2);
        memo.insert(t, s.clone());
        return s;
      }
    }
  }
  memo.insert(t, vec![]);
  vec![]
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    lines.next();
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let n = xs[0];
    let m = xs[1];
    let mut css: Vec<String> = vec![];
    let mut sub = BTreeMap::<&str, (usize, usize, usize)>::new();
    for _ in 0..n {
      css.push(lines.next().unwrap().unwrap());
    }
    for i in 0..n {
      let cs = &css[i];
      for j in 0..=(m - 2) {
        for k in (j + 2)..=m {
          let t = &cs[j..k];
          let b = !sub.contains_key(t);
          if b {
            let v = (j, k, i);
            sub.insert(t, v);
          }
        }
      }
    }
    let s: String = lines.next().unwrap().unwrap();
    let mut memo = BTreeMap::<&str, Vec<(usize, usize, usize)>>::new();
    let ans = find_substring(&s, &css, &sub, &mut memo);
    if ans.is_empty() {
      println!("-1");
    } else {
      println!("{}", ans.len());
      for (j, k, i) in ans {
        println!("{} {} {}", j + 1, k, i + 1);
      }
    }
  }
}