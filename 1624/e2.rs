use std::io::{self, BufRead};
use std::collections::BTreeMap;

fn find_substring<'a>(
  t: &'a str,
  sub: &BTreeMap<&'a str, (usize, usize, usize)>,
  memo: &mut BTreeMap<&'a str, Vec<(usize, usize, usize)>>
) -> Vec<(usize, usize, usize)> {
  if let Some(v) = memo.get(t) {
    return v.clone();
  }
  if let Some(&v) = sub.get(t) {
    let vs = vec![v];
    memo.insert(t, vs.clone());
    return vs;
  }
  let l = t.len();
  if 4 <= l {
    for k in 2..=(l - 2) {
      let v1 = find_substring(&t[..k], sub, memo);
      let v2 = find_substring(&t[k..], sub, memo);
      if !v1.is_empty() && !v2.is_empty() {
        let mut s = v1.clone();
        let mut s2 = v2.clone();
        s.append(&mut s2);
        memo.insert(t, s.clone());
        return s;
      }
    }
  }
  memo.insert(t, vec![]);
  vec![]
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    lines.next();
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let n = xs[0];
    let m = xs[1];
    let mut css: Vec<String> = vec![];
    let mut sub = BTreeMap::<&str, (usize, usize, usize)>::new();
    for _ in 0..n {
      css.push(lines.next().unwrap().unwrap());
    }
    for i in 0..n {
      let cs = &css[i];
      for j in 0..=(m - 2) {
        for k in (j + 2)..=m {
          let t = &cs[j..k];
          let b = !sub.contains_key(t);
          if b {
            let v = (j, k, i);
            sub.insert(t, v);
          }
        }
      }
    }
    let s: String = lines.next().unwrap().unwrap();
    let mut memo = BTreeMap::<&str, Vec<(usize, usize, usize)>>::new();
    for k in 2..=(m - 2) {
      for i in 0..=(m - k) {
        let j = i + k;
        let t = &s[i..j];
        find_substring(t, &sub, &mut memo);
      }
    }
    let ans = find_substring(&s[..], &sub, &mut memo);
    if ans.is_empty() {
      println!("-1");
    } else {
      println!("{}", ans.len());
      for (i, j, k) in ans {
        println!("{} {} {}", i + 1, j, k + 1);
      }
    }
  }
}