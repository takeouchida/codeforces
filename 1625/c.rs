use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let n = xs[0] as usize;
  let l = xs[1];
  let k = xs[2] as usize;
  let ds: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let xs: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let mut memo: Vec<Vec<(u64, u64)>> = vec![vec![(u64::MAX, 0); k + 1]; n];
  let mut ls: Vec<u64> = vec![0; n];
  for i in 0..(n - 1) {
    ls[i] = ds[i + 1] - ds[i];
  }
  ls[n - 1] = l - ds[n - 1];
  memo[0][0] = (ls[0] * xs[0], xs[0]);
  for i in 1..n {
    memo[i][0] = (memo[i - 1][0].0 + ls[i] * xs[i], xs[i]);
    for j in 1..=k {
      let d1 = if memo[i - 1][j - 1].0 == u64::MAX { u64::MAX } else { memo[i - 1][j - 1].0 + ls[i] * memo[i - 1][j - 1].1 };
      let d2 = if memo[i - 1][j].0 == u64::MAX { u64::MAX } else { memo[i - 1][j].0 + ls[i] * xs[i] };
      if d1 < d2 {
        memo[i][j] = (d1, memo[i - 1][j - 1].1);
      } else {
        memo[i][j] = (d2, xs[i]);
      }
    }
  }
  let ans = (0..=k).map(|i| memo[n - 1][i].0).min().unwrap();
  println!("{}", ans);
}