use std::io::{self, BufRead};
use std::cmp::min;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let n = xs[0] as usize;
  let l = xs[1];
  let k = xs[2] as usize;
  let mut a: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let b: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let mut dp: Vec<Vec<u64>> = vec![vec![u64::MAX; k + 1]; n + 1];
  a.push(l);
  dp[0][0] = 0;
  for i in 1..=n {
    dp[i][0] = dp[i - 1][0] + b[i - 1] * (a[i] - a[i - 1]);
  }
  for i in 0..n {
    for j in 0..=k {
      let pos_max = min(n, k + i + 1 - j);
      for pos in (i + 2)..=pos_max {
        if dp[i][j] == u64::MAX {
          continue;
        }
        dp[pos][j + pos - i - 1] = min(dp[pos][j + pos - i - 1], dp[i][j] + b[i] * (a[pos] - a[i]))
      }
    }
  }
  let ans = (0..=k).map(|j| dp[n][j]).min().unwrap();
  println!("{}", ans);
}