use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let cs: Vec<char> = lines.next().unwrap().unwrap().chars().collect();
    let l = cs.len();
    let mut d = None;
    for i in (1..l).rev() {
      let a = (cs[i]  as usize - '0' as usize) + (cs[i - 1] as usize - '0' as usize);
      if a >= 10 {
        d = Some(i);
        break;
      }
    }
    match d {
      None => {
        print!("{}", (cs[0] as usize - '0' as usize) + (cs[1] as usize) - '0' as usize);
        println!("{}", cs[2..l].iter().cloned().collect::<String>());
      },
      Some(e) => {
        print!("{}", cs[0..(e - 1)].iter().cloned().collect::<String>());
        print!("{}", (cs[e - 1] as usize - '0' as usize) + (cs[e] as usize) - '0' as usize);
        println!("{}", cs[(e + 1)..l].iter().cloned().collect::<String>());
      },
    }
  }
}