use std::io::{self, BufRead};
use std::collections::BTreeMap;
use std::cmp::min;

pub fn upper_bound(vec: &[(u64, u64)], val: u64) -> u64 {
  let mut lo = 0;
  let mut hi = vec.len();
  while hi - lo > 0 {
    let p = (hi + lo) / 2;
    if vec[p].1 > val {
      hi = p;
    } else {
      lo = p + 1;
    }
  }
  return lo as u64;
}

pub fn find_people(val: u64) -> u64 {
  let mut lo: u32 = 0;
  let mut hi: u32 = 31;
  while hi - lo > 0 {
    let p = (hi + lo) / 2;
    if 2_u64.pow(p) < val {
      lo = p + 1;
    } else {
      hi = p;
    }
  }
  return 2_u64.pow(lo) - val;
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let _: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let xs: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let mut counts: BTreeMap<u64, u64> = BTreeMap::new();
    for x in xs {
      *counts.entry(x).or_insert(0) += 1;
    }
    let mut counts: Vec<(u64, u64)> = counts.into_iter().collect();
    let mut c = counts[0].1;
    for i in 1..counts.len() {
      counts[i].1 += c;
      c = counts[i].1;
    }
    let mx = counts[counts.len() - 1].1;
    let mut p: u32 = 0;
    let mut ans = u64::MAX;
    loop {
      let i = upper_bound(&counts, 2_u64.pow(p));
      let s = if i == 0 { 0 } else { counts[i as usize - 1].1 };
      let mut q = 0;
      loop {
        let j = upper_bound(&counts, 2_u64.pow(p) + 2_u64.pow(q));
        let t = if j == 0 { 0 } else { counts[j as usize - 1].1 };
        let mn = find_people(s) + find_people(t - s) + find_people(mx - t);
        ans = min(ans, mn);
        q += 1;
        if mx < 2_u64.pow(p) + 2_u64.pow(q) {
          break;
        }
      }
      p += 1;
      if mx < 2_u64.pow(p) {
        break;
      }
    }
    println!("{}", ans);
  }
}