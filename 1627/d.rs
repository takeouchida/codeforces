use std::io::{self, BufRead};
use std::collections::BTreeMap;
use std::collections::BTreeSet;

fn gcd(x: usize, y: usize) -> usize {
  let (mut a, mut b) = if x < y {
    (y, x)
  } else {
    (x, y)
  };
  while b != 0 {
    let r = a % b;
    a = b;
    b = r;
  }
  a
}

pub fn find_prime_factors(n: usize) -> BTreeMap<usize, usize> {
  let mut res = BTreeMap::new();
  let mut i = 2;
  let mut m = n;
  while i * i <= m {
    while m % i == 0 {
      let val = 1 + res.get(&i).unwrap_or(&0);
      res.insert(i, val);
      m /= i;
    }
    i += 1
  }
  if m != 1 {
    let val = 1 + res.get(&m).unwrap_or(&0);
    res.insert(m, val);
  }
  res
}

fn find_divisors(n: usize) -> Vec<usize> {
  let fs = find_prime_factors(n);
  let mut ds = vec![1];
  for (f, m) in fs {
    let mut x = 1;
    let mut es = vec![1];
    for _ in 0..m {
      x *= f;
      es.push(x);
    }
    let mut gs = vec![];
    for d in ds {
      for e in &es {
        gs.push(d * *e);
      }
    }
    ds = gs;
  }
  return ds;
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let mut xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let mut checked: BTreeSet<usize> = xs.iter().cloned().collect();
  let mut candidates: BTreeMap<usize, BTreeSet<usize>> = BTreeMap::new();
  let mut p = 0;
  while p < xs.len() {
    let x = xs[p];
    let ds = find_divisors(x);
    checked.insert(x);
    for d in ds {
      if checked.contains(&d) {
        continue;
      }
      match candidates.get(&d) {
        None => {
          let mut ys = BTreeSet::new();
          ys.insert(x);
          candidates.insert(d, ys);
        },
        Some(ys) => {
          if ys.iter().any(|y| gcd(x, *y) == d) {
            candidates.remove(&d);
            if !checked.contains(&d) {
              xs.push(d);
              checked.insert(d);
            }
          } else {
            let mut zs = ys.clone();
            zs.insert(x);
            candidates.insert(d, zs);
          }
        },
      }
    }
    p += 1;
  }
  println!("{}", xs.len() - n);
}