use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let mut grid: Vec<Vec<usize>> = vec![];
    for _ in 0..n {
      let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
      grid.push(xs);
    }
    let mut orig: Vec<Vec<Option<usize>>> = vec![vec![None; n]; n];
    for i in 0..(n - 1) {
      for j in 0..n {
        let u = if i == 0 { Some(0) } else { orig[i - 1][j] };
        let l = if j == 0 { Some(0) } else { orig[i][j - 1] };
        let r = if j == n - 1 { Some(0) } else { orig[i][j + 1] };
        let d = orig[i + 1][j];
        let g = grid[i][j];
        let total = [u, l, r, d].iter().filter(|e| e.is_some()).fold(g, |x, y| x ^ y.unwrap());
        if l.is_none() {
          orig[i][j - 1] = Some(total);
          if j < n - 1 && r.is_none() {
            orig[i][j + 1] = Some(0);
          }
          orig[i + 1][j] = Some(0);
        } else if r.is_none() {
          orig[i][j + 1] = Some(total);
          orig[i + 1][j] = Some(0);
        } else {
          orig[i + 1][j] = Some(total);
        }
      }
    }
    let ans = orig.iter().flatten().map(|e| e.unwrap()).fold(0, |x, y| x ^ y);
    println!("{}", ans);
  }
}