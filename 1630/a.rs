use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let n = xs[0];
    let k = xs[1];
    let mask = n - 1;
    if k == mask {
      if n == 4 {
        println!("-1");
      } else {
        println!("0 {}", mask - 1);
        for i in 1..(n / 2 - 1) {
          let mut m = 1;
          let mut b = true;
          while m <= i {
            if i == m {
              println!("{} {}", i, n - 2 * i - 1);
              b = false;
            }
            m *= 2;
          }
          if b {
            println!("{} {}", i, n - i - 1);
          }
        }
        println!("{} {}", n - 1, n / 2);
      }
    } else {
      println!("{} {}", k, mask);
      for i in 1..(n / 2) {
        if i == k {
          println!("0 {}", n - k - 1);
        } else if i == n - k - 1 {
          println!("{} 0", n - k - 1);
        } else {
          println!("{} {}", i, n - i - 1);
        }
      }
    }
  }
}