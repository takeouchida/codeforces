use std::io::{self, BufRead};
use std::collections::BTreeMap;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let n = xs[0];
    let k = xs[1];
    let a: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let mut hist: BTreeMap<usize, usize> = BTreeMap::new();
    for x in a {
      *hist.entry(x).or_insert(0) += 1;
    }
    let m = hist.len();
    let mut table = vec![(0, 0); m + 1];
    let mut i = 1;
    for (k, v) in hist {
      table[i] = (k, table[i - 1].1 + v);
      i += 1;
    }
    println!("{:?}", table);
    let border = (table[m].1 - k + 1) / 2 + 1;
    let mut width = table[m].1;
    let mut i = 0;
    let mut j = 1;
    let mut i_min = table[1].0;
    let mut j_min = table[m].0;
    while j <= m {
      let diff = table[j].1 - table[i].1;
      if border <= diff {
        if diff < width {
          width = diff;
          i_min = table[i + 1].0;
          j_min = table[j].0;
          i += 1;
        } else {
          i += 1;
        }
      } else {
        j += 1;
      }
    }
    println!("{} {} {}", i_min, j_min, width);
  }
}