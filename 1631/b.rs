use std::io::{self, BufRead};
use std::collections::BTreeSet;

pub fn lower_bound<T: Ord>(vec: &[T], val: T) -> usize {
  let mut lo = 0;
  let mut hi = vec.len();
  while hi - lo > 0 {
    let p = (hi + lo) / 2;
    if vec[p] < val {
      lo = p + 1;
    } else {
      hi = p;
    }
  }
  return lo;
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let ks: BTreeSet<_> = xs.iter().collect();
    let ks: Vec<_> = ks.iter().collect();
    let xs: Vec<_> = xs.iter().rev().map(|x| lower_bound(&ks, &x)).collect();
    let mut memo: Vec<usize> = vec![0; n];
    let h = xs[0];
    for i in 1..n {
      if xs[i] == h {
        memo[i] = memo[i - 1];
      } else {
        memo[i] = 1 + memo[(i / 2)..i].iter().min().unwrap();
      }
    }
    println!("{}", memo[n - 1]);
  }
}