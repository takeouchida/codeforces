use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let mut m = 1 << 18;
    while (n - 1) & m == 0 {
      m >>= 1;
    }
    let mut ans = vec![];
    for i in 0..m {
      ans.push(m - i - 1);
    }
    for i in m..n {
      ans.push(i);
    }
    print!("{}", ans[0]);
    for i in 1..n {
      print!(" {}", ans[i]);
    }
    println!();
  }
}