use std::io::{self, BufRead};
use std::cmp::min;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let mut a = xs[0];
    let b = xs[1];
    let mut ans1 = 0;
    loop {
      if a == b {
        break;
      }
      if a | b == b {
        ans1 += 1;
        break;
      }
      a += 1;
      ans1 += 1;
    }
    let a = xs[0];
    let mut b = xs[1];
    let mut ans2 = 0;
    for _ in 0..ans1 {
      if a == b {
        break;
      }
      if a | b == b {
        ans2 += 1;
        break;
      }
      b += 1;
      ans2 += 1;
    }
    println!("{}", min(ans1, ans2));
  }
}