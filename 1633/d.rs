use std::io::{self, BufRead};
use std::cmp::max;

fn find_num_ops(mut b: usize, c: usize) -> usize {
  if c < b {
    return 0
  }
  let mut count = 0;
  while b < c {
    let d = c - b;
    let e = b / (b / (d + 1) + 1);
    count += 1;
    b += e;
  }
  count
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let n = xs[0];
    let k = xs[1];
    let bs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let cs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let ops: Vec<_> = (0..n).map(|i| find_num_ops(1, bs[i])).collect();
    let costs: Vec<_> = ops.iter().zip(&cs).collect();
    let mut memo = vec![vec![0; k + 1]; n + 1];
    for i in 1..=n {
      for c in 0..=k {
        let ci = costs[i - 1].0;
        if c < *ci {
          memo[i][c] = memo[i - 1][c];
        } else {
          memo[i][c] = max(memo[i - 1][c], memo[i - 1][c - ci] + costs[i - 1].1);
        }
      }
    }
    println!("{}", memo[n][k])
  }
}