use std::io::{self, BufRead};
use std::collections::BTreeSet;

fn union_find(roots: &mut Vec<usize>, x: usize) -> usize {
  let y = roots[x];
  if x == y {
    x
  } else {
    let z = union_find(roots, y);
    roots[x] = z;
    z
  }
}

fn solve(es: &Vec<Vec<BTreeSet<i64>>>, x: i64, n: usize) -> i64 {
  let mut costs: Vec<(i64, usize, usize)> = vec![];
  for i in 0..n {
    for j in 0..n {
      let ws = &es[i][j];
      if ws.is_empty() {
        continue;
      }
      costs.push((ws.iter().map(|w| (w - x).abs()).min().unwrap(), i, j));
    }
  }
  costs.sort();
  let mut roots: Vec<usize> = (0..n).collect();
  let mut ans = 0;
  for (w, u, v) in costs {
    let ru = union_find(&mut roots, u);
    let rv = union_find(&mut roots, v);
    if ru == rv {
      continue;
    }
    ans += w;
    roots[rv] = ru;
  }
  ans
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let n = xs[0];
  let m = xs[1];
  let mut es: Vec<Vec<BTreeSet<i64>>> = vec![vec![BTreeSet::new(); n]; n];
  for _ in 0..m {
    let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let v = xs[0] - 1;
    let u = xs[1] - 1;
    let w = xs[2] as i64;
    es[v][u].insert(w);
    es[u][v].insert(w);
  }
  let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let p = xs[0];
  let k = xs[1];
  let a = xs[2];
  let b = xs[3];
  let c = xs[4];
  let qs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let mut ans = 0;
  for q in &qs {
    ans = ans ^ solve(&es, *q as i64, n);
  }
  let mut q = qs[p - 1];
  for _ in 0..(k - p) {
    q = (q * a + b) % c;
    ans = ans ^ solve(&es, q as i64, n);
  }
  println!("{}", ans);
}