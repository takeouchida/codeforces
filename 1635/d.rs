use std::io::{self, BufRead};
use std::collections::BTreeSet;

const M: u64 = 1000000007;

pub fn lower_bound<T: Ord>(vec: &[T], val: T) -> usize {
  let mut lo = 0;
  let mut hi = vec.len();
  while hi - lo > 0 {
    let p = (hi + lo) / 2;
    if vec[p] < val {
      lo = p + 1;
    } else {
      hi = p;
    }
  }
  return lo;
}

fn match_00_or_1(s: &[u8]) -> bool {
  let mut st = 0;
  for i in 0..s.len() {
    if st == 0 {
      if s[i] == '0' as u8 {
        st = 1;
      } else if s[i] == '1' as u8 {
        st = 0;
      } else {
        return false;
      }
    } else if st == 1 {
      if s[i] == '0' as u8 {
        st = 0;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }
  st == 0
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let x: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let p = x[1];
  let mut a: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let mut b: Vec<u64> = vec![0; p + 1];
  let mut c: Vec<u64> = vec![0; p + 1];
  a.sort();
  let a_bin: Vec<Vec<u8>> = a.iter().map(|y| format!("{:b}", y).as_bytes().to_vec()).collect();
  let mut blacklist: BTreeSet<u64> = BTreeSet::new();
  for &x in &a {
    let prefix = format!("{:b}", x);
    let prefix_len = prefix.len();
    for i in 1..32 {
      if (1 << 32) < (x << i) {
        break;
      }
      let lo = lower_bound(&a, x << i);
      let hi = lower_bound(&a, (x + 1) << i);
      for i in lo..hi {
        if match_00_or_1(&a_bin[i][prefix_len..]) {
          blacklist.insert(a[i]);
        }
      }
    }
  }
  for &x in &a {
    for i in 0..=p {
      if x < (1 << i) {
        if !blacklist.contains(&x) {
          b[i] += 1;
        }
        break;
      }
    }
  }
  c[0] = b[0];
  c[1] = b[1] + c[0];
  for i in 2..=p {
    c[i] = (b[i] + c[i - 2] + c[i - 1]) % M;
  }
  let ans: u64 = c.iter().fold(0, |a, b| (a + b) % M);
  println!("{}", ans);
}