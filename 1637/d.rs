use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let a: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let b: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let mut memo: Vec<usize> = vec![0; n];
    let mut sum: Vec<Vec<usize>> = vec![vec![0; 2]; n];
    sum[0][0] = a[0];
    sum[0][1] = b[0];
    for i in 1..n {
      let x = sum[i - 1][0] * a[i] + sum[i - 1][1] * b[i];
      let y = sum[i - 1][0] * b[i] + sum[i - 1][1] * a[i];
      if x < y {
        memo[i] = memo[i - 1] + x;
        sum[i][0] = sum[i - 1][0] + a[i];
        sum[i][1] = sum[i - 1][1] + b[i];
      } else {
        memo[i] = memo[i - 1] + y;
        sum[i][0] = sum[i - 1][0] + b[i];
        sum[i][1] = sum[i - 1][1] + a[i];
      }
    }
    println!("{}", memo[n - 1]
      + a.iter().map(|x| x * x).sum::<usize>() * (n - 1)
      + b.iter().map(|x| x * x).sum::<usize>() * (n - 1));
  }
}