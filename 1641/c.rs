use std::io::{self, BufRead};

pub fn upper_bound<T>(vec: &[(usize, T)], t: usize) -> usize {
  let mut lo = 0;
  let mut hi = vec.len();
  while hi - lo > 0 {
    let p = (hi + lo) / 2;
    if vec[p].0 > t {
      hi = p;
    } else {
      lo = p + 1;
    }
  }
  return lo;
}

#[derive(Debug)]
struct BIT {
  tree: Vec<Vec<(usize, usize)>>
}

impl BIT {
  pub fn new(n: usize) -> BIT {
    BIT { tree: vec![vec![]; n + 1] }
  }

  pub fn sum(&self, t: usize, i: usize) -> usize {
    assert!(i < self.tree.len());
    let mut j = i as isize;
    let mut s: usize = 0;
    while j > 0 { 
      let v = &self.tree[j as usize];
      let k = upper_bound(v, t);
      s += if k == 0 { 0 } else { v[k - 1].1 };
      j -= j & -j; 
    }   
    s   
  }

  pub fn add(&mut self, t: usize, i: usize, x: usize) {
    assert!(i < self.tree.len());
    let mut j = i as isize;
    while j < self.tree.len() as isize {
      let v = &mut self.tree[j as usize];
      let k = upper_bound(v, t);
      let val = if k == 0 { 0 } else { v[k - 1].1 };
      if 0 < k && v[k - 1].0 == t {
        v[k - 1].1 += x;
      } else {
        v.insert(k, (t, val + x))
      }
      j += j & -j; 
    }   
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let x: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let n = x[0];
  let q = x[1];
  let mut t = 0;
  let mut query: Vec<(usize, usize)> = vec![];
  let mut bit = BIT::new(n);
  let mut bounds: Vec<Vec<(usize, (usize, usize))>> = vec![vec![]; n];
  for _ in 0..q {
    let a: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    if a[0] == 0 {
      let l = a[1] - 1;
      let r = a[2] - 1;
      let x = a[3];
      t += 1;
      if x == 0 {
        for i in l..=r {
          bit.add(t, i, 1);
        }
      } else if x == 1 {
        for i in l..=r {
          bounds[i].push((t, (l, r)));
        }
      }
    } else if a[0] == 1 {
      let j = a[1] - 1;
      query.push((t, j));
    }
  }
}