use std::io::{self, BufRead};
use std::collections::BTreeSet;

const M: u64 = 998244353;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let a: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let n = a[0];
    let m = a[1];
    let k = a[2] as u64;
    let q = a[3];
    let mut ps: Vec<(usize, usize)> = vec![];
    for _ in 0..q {
      let a: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
      ps.push((a[0], a[1]));
    }
    ps = ps.into_iter().rev().collect();
    let mut xs: BTreeSet<usize> = (1..=m).collect();
    let mut ys: BTreeSet<usize> = (1..=n).collect();
    let mut c = 0;
    for &(x, y) in &ps {
      if xs.is_empty() || ys.is_empty() {
        break;
      }
      if xs.contains(&x) || ys.contains(&y) {
        c += 1;
      }
      xs.remove(&x);
      ys.remove(&y);
    }
    let mut ans: u64 = 1;
    for _ in 0..c {
      ans = (ans * k) % M
    }
    println!("{}", ans);
  }
}