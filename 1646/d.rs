use std::io::{self, BufRead};
use std::collections::BTreeSet;

struct State {
  v0: BTreeSet<usize>,
  v1: BTreeSet<usize>,
}

impl State {
  fn new() -> State {
    State { v0: BTreeSet::new(), v1: BTreeSet::new() }
  }

  fn dfs(&mut self, u: usize, e: &Vec<Vec<usize>>, is_v0: bool) {
    if self.v0.contains(&u) || self.v1.contains(&u) {
      return
    }
    if is_v0 {
      self.v0.insert(u);
    } else {
      self.v1.insert(u);
    }
    for v in &e[u] {
      self.dfs(*v, e, !is_v0);
    }
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let mut e: Vec<Vec<usize>> = vec![vec![]; n];
  for _ in 0..(n - 1) {
    let x: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let u = x[0] - 1;
    let v = x[1] - 1;
    e[u].push(v);
    e[v].push(u);
  }
  let mut s = State::new();
  s.dfs(0, &e, true);
  let (b, w) = if s.v0.len() < s.v1.len() { (s.v1, s.v0) } else { (s.v0, s.v1) };
  let mut v: Vec<usize> = (0..n).map(|u| if w.contains(&u) { 1 } else { 0 } ).collect();
  for &u in &b {
    v[u] = e[u].iter().count();
  }
  let count: usize = (0..n).filter(|&u| e[u].iter().map(|&w| v[w]).sum::<usize>() == v[u] ).count();
  let sum: usize = v.iter().sum();
  println!("{} {}", count, sum);
  for u in v {
    print!("{} ", u);
  }
  println!();
}