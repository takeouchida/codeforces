use std::io::{self, BufRead};
use std::collections::BTreeMap;

fn gcd(x: usize, y: usize) -> usize {
  let (mut a, mut b) = if x < y {
    (y, x)
  } else {
    (x, y)
  };
  while b != 0 {
    let r = a % b;
    a = b;
    b = r;
  }
  a
}

pub fn find_prime_factors(n: usize) -> BTreeMap<usize, usize> {
  let mut res = BTreeMap::new();
  let mut i = 2;
  let mut m = n;
  while i * i <= m {
    while m % i == 0 {
      let val = 1 + res.get(&i).unwrap_or(&0);
      res.insert(i, val);
      m /= i;
    }
    i += 1
  }
  if m != 1 {
    let val = 1 + res.get(&m).unwrap_or(&0);
    res.insert(m, val);
  }
  res
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let x: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let n = x[0];
  let m = x[1];
  let mut count: usize = 1;
  for i in 2..=n {
    let f = find_prime_factors(i);
    if f.len() == 1 {
      let k = *f.keys().next().unwrap();
      let v = f[&k];
      if v == 1 {
        count += m;
      } else {
        count += m - m / k;
      }
    } else {
      let mut ks = f.values();
      let mut p = gcd(*ks.next().unwrap(), *ks.next().unwrap());
      for k in ks {
        p = gcd(p, *k);
      }
      if p == 1 {
        count += m;
      } else {
        count += m - m / p
      }
    }
  }
  println!("{}", count);
}