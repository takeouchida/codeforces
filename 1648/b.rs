use std::io::{self, BufRead};

pub fn lower_bound<T: Ord>(vec: &[T], val: T) -> usize {
  let mut lo = 0;
  let mut hi = vec.len();
  while hi - lo > 0 {
    let p = (hi + lo) / 2;
    if vec[p] < val {
      lo = p + 1;
    } else {
      hi = p;
    }
  }
  return lo;
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let x: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let n = x[0];
    let mut a: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    a.sort();
    // println!("{:?}", a);
    let a_max = a[n - 1];
    let mut p = 0;
    let mut integral = true;
    for n in 1..a_max {
      if a[p] == i {
        p += 1;
        continue;
      }
      for q in 0..n {
        if a_max < i * a[q] {
          break;
        }
        // println!("  {} {}", i * a[q], (i + 1) * a[q]);
        let lo = lower_bound(&a, i * a[q]);
        let hi = lower_bound(&a, (i + 1) * a[q]);
        if lo < hi {
          integral = false;
          break;
        }
      }
      if !integral {
        break;
      }
    }
    println!("{}", if integral { "Yes" } else { "No" });
  }
}