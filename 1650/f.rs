use std::io::{self, BufRead};
use std::collections::BTreeMap;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let x: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let n = x[0];
    let m = x[1];
    let a: Vec<u32> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let mut opts: Vec<Vec<(usize, u32, u32)>> = vec![vec![]; n]; // (job x option_index) -> (option x time x percent)
    for i in 0..m {
      let x: Vec<u32> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
      let e: usize = x[0] as usize - 1;
      let t = x[1];
      let p = x[2];
      opts[e].push((i + 1, t, p));
    }
    let mut total_time = 0;
    let mut can_solve = true;
    let mut answer: Vec<usize> = vec![];
    for i in 0..n {
      let limit = a[i] - total_time;
      let n_jobs = opts[i].len();
      let mut dp: Vec<Vec<(u32, u32, Vec<usize>)>> = vec![vec![]; n_jobs + 1]; // option -> [(time x percent x [option])]
      dp[0] = vec![(0, 0, vec![])];
      let mut ans: Option<(u32, Vec<usize>)> = None;
      for j in 0..n_jobs {
        let mut slice: BTreeMap<u32, (u32, Vec<usize>)> = dp[j].iter().cloned().map(|(t, p, xs)| (t, (p, xs))).collect();
        for (t, p, xs) in &dp[j] {
          let u = t + opts[i][j].1;
          let s = p + opts[i][j].2;
          let mut ys = xs.clone();
          ys.push(opts[i][j].0);
          if s >= 100 && u <= limit {
            ans = Some((u, ys));
            break;
          }
          if !slice.contains_key(&u) {
            slice.insert(u, (s, ys));
          }
        }
        if ans.is_some() {
          break;
        }
        dp[j + 1] = slice.into_iter().map(|(t, (p, xs))| (t, p, xs)).collect();
      }
      match ans {
        None => {
          can_solve = false;
          break;
        },
        Some((t, xs)) => {
          total_time += t;
          for x in xs {
            answer.push(x);
          }
        },
      }
      if a[i] < total_time {
        can_solve = false;
        break;
      }
    }
    if can_solve {
      println!("{}", answer.len());
      for x in answer {
        print!("{} ", x);
      }
      println!();
    } else {
      println!("-1");
    }
  }
}