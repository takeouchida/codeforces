use std::io::{self, BufRead};
use std::collections::{BinaryHeap, BTreeSet};
use std::cmp::Ordering;

pub fn lower_bound(vec: &[(u64, usize)], val: u64) -> usize {
  let mut lo = 0;
  let mut hi = vec.len();
  while hi - lo > 0 {
    let p = (hi + lo) / 2;
    if vec[p].0 < val {
      lo = p + 1;
    } else {
      hi = p;
    }
  }
  return lo;
}

fn abs_diff(a: u64, b: u64) -> u64 {
  if a < b { b - a } else { a - b }
}

#[derive(PartialEq, Eq, Debug, Copy, Clone, Default, Hash)]
struct Elem {
  diff: u64,
  i: usize,
  j: usize,
}

impl PartialOrd for Elem {
  fn partial_cmp(&self, other: &Elem) -> Option<Ordering> {
    other.diff.partial_cmp(&self.diff)
  }
}

impl Ord for Elem {
  fn cmp(&self, other: &Elem) -> Ordering {
    other.diff.cmp(&self.diff)
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let a: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let b: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    // let mut b_sorted: Vec<(u64, usize)> = b.iter().cloned().enumerate().map(|(i, x)| (x, i)).collect();
    // b_sorted.sort();
    // let mut priq: BinaryHeap<Elem> = BinaryHeap::new();
    // let mut blacklist: BTreeSet<(usize, usize)> = BTreeSet::new();
    // for i in 0..n {
    //   blacklist.insert((i, i));
    //   if i != 0 {
    //     priq.push(Elem { diff: abs_diff(a[i], b[i - 1]), i: i, j: i - 1 });
    //   }
    //   if i != n - 1 {
    //     priq.push(Elem { diff: abs_diff(a[i], b[i + 1]), i: i, j: i + 1 });
    //   }
    //   let p = lower_bound(&b_sorted, a[i]);
    //   if p != n && !blacklist.contains(&(i, p)) {
    //     priq.push(Elem { diff: abs_diff(a[i], b[p]), i: i, j: p});
    //   }
    //   if p != 0 && !blacklist.contains(&(i, p - 1)) {
    //     priq.push(Elem { diff: abs_diff(a[i], b[p - 1]), i: i, j: p - 1 });
    //   }
    // }
    // let mut ans = 0;
    // let mut cands_a: BTreeSet<usize> = (0..n).collect();
    // let mut cands_b: BTreeSet<usize> = (0..n).collect();
    // while !cands_a.is_empty() && !priq.is_empty() {
    //   let e = priq.pop().unwrap();
    //   println!("{:?}", e);
    //   if cands_a.contains(&e.i) && cands_b.contains(&e.j) && !blacklist.contains(&(e.i, e.j)) {
    //     ans += e.diff;
    //     cands_a.remove(&e.i);
    //     cands_b.remove(&e.j);
    //     blacklist.insert((e.i, e.j));
    //     if e.j != n - 1 && !blacklist.contains(&(e.i, e.j + 1)) {
    //       priq.push(Elem { diff: abs_diff(a[e.i], b[e.j + 1]), i: e.i, j: e.j + 1 });
    //     }
    //     if e.j != 0 && !blacklist.contains(&(e.i, e.j - 1)) {
    //       priq.push(Elem { diff: abs_diff(a[e.i], b[e.j - 1]), i: e.i, j: e.j - 1 });
    //     }
    //   }
    // }
    // println!("{}", ans);
  }
}