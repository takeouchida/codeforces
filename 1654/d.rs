use std::io::{self, BufRead};
use std::collections::BTreeSet;

#[derive(Debug)]
struct State {
  e: Vec<Vec<(usize, u64, u64)>>,
}

fn gcd(x: u64, y: u64) -> u64 {
  let (mut a, mut b) = if x < y {
    (y, x)
  } else {
    (x, y)
  };
  while b != 0 {
    let r = a % b;
    a = b;
    b = r;
  }
  a
}

impl State {
  fn new(n: usize) -> State {
    State { e: vec![vec![]; n] }
  }

  fn dfs(&self) {
    let mut visited = BTreeSet::<usize>::new();
    self.dfs_aux(0, 1, &mut visited);
  }

  fn dfs_aux(&self, u: usize, a: u64, visited: &mut BTreeSet<usize>) -> u64 {
    visited.insert(u);
    let mut d = a;
    let mut vs: Vec<u64> = vec![];
    for (v, x, y) in &self.e[u] {
      if visited.contains(v) {
        continue;
      }
      println!("> {} {} {} {}", u, v, x, y);
      let m = self.dfs_aux(*v, *y, visited);
      vs.push(*x * m);
      d = gcd(d, *x);
    }
    let ret = vs.iter().fold(1, |x, y| x * y / d);
    println!("{} {} {}", u, d, ret);
    ret
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let mut st = State::new(n);
    for _ in 0..(n - 1) {
      let xs: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
      let i = xs[0] - 1;
      let j = xs[1] - 1;
      let x = xs[2] as u64;
      let y = xs[3] as u64;
      let d = gcd(x, y);
      let x = x / d;
      let y = y / d;
      st.e[i].push((j, x, y));
      st.e[j].push((i, y, x));
    }
    println!("----");
    st.dfs()
  }
}