use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let a: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let x = 1 + a.iter().enumerate().max_by(|l, r| l.1.cmp(r.1)).unwrap().0;
    let y = 1 + a.iter().enumerate().min_by(|l, r| l.1.cmp(r.1)).unwrap().0;
    println!("{} {}", y, x);
  }
}