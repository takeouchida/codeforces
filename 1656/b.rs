use std::io::{self, BufRead};
use std::cmp::Ordering;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let x: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let n = x[0];
    let k = x[1] as i64;
    let mut x: Vec<i64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    x.sort();
    let y: Vec<i64> = x.iter().map(|e| e + k).collect();
    let mut i = 0;
    let mut j = 0;
    let mut found = false;
    while i < n && j < n {
      match x[i].cmp(&y[j]) {
        Ordering::Less => { i += 1; },
        Ordering::Equal => {
          found = true;
          break;
        },
        Ordering::Greater => { j += 1; },
      }
    }
    println!("{}", if found { "YES" } else { "NO" });
  }
}