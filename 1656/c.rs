use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    if n == 1 {
      println!("YES");
      continue;
    }
    let mut a: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    a.sort();
    let mut i = 1;
    let mut d = 0;
    let mut p = 0;
    let mut possible = true;
    while i < n {
      if a[i] - a[i - 1] == 1 {
        possible = false;
        break;
      }
      if a[i] - a[i - 1] > 1 {
        d = a[i] - a[i - 1];
        p = a[i - 1] % d;
        break;
      }
      i += 1;
    }
    if i == n {
      println!("YES");
      continue;
    }
    if !possible {
      println!("NO");
      continue;
    }
    while i < n {
      let m = a[i] % d;
      if m != p && m != 0 {
        possible = false;
        break;
      }
      i += 1;
    }
    println!("{}", if possible { "YES" } else { "NO" });
  }
}