use std::io::{self, BufRead};
use std::collections::BTreeMap;

pub fn find_prime_factors(n: u64) -> BTreeMap<u64, u64> {
  let mut res = BTreeMap::new();
  let mut i = 2;
  let mut m = n;
  while i * i <= m {
    while m % i == 0 {
      let val = 1 + res.get(&i).unwrap_or(&0);
      res.insert(i, val);
      m /= i;
    }
    i += 1
  }
  if m != 1 {
    let val = 1 + res.get(&m).unwrap_or(&0);
    res.insert(m, val);
  }
  res
}

fn find_divisors(n: u64) -> Vec<u64> {
  let fs = find_prime_factors(n);
  let mut ds = vec![1];
  for (f, m) in fs {
    let mut x = 1;
    let mut es = vec![1];
    for _ in 0..m {
      x *= f;
      es.push(x);
    }
    let mut gs = vec![];
    for d in ds {
      for e in &es {
        gs.push(d * *e);
      }
    }
    ds = gs;
  }
  return ds;
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: u64 = lines.next().unwrap().unwrap().parse().unwrap();
    let ks = find_divisors(n);
    let mut k_good = None;
    for k in ks {
      if k == 1 {
        continue;
      }
      if k % 2 == 0 {
        let q = n / k;
        if k < q && (q - k) % 2 == 1 {
          k_good = Some(k);
          break;
        }
      } else {
        let p = (k - 1) / 2;
        let q = n / k;
        if q > p {
          k_good = Some(k);
          break;
        }
      }
    }
    match k_good {
      None => println!("-1"),
      Some(k) => println!("{}", k),
    }
  }
}