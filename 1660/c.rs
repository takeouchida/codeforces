use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let s = lines.next().unwrap().unwrap();
    let bs = s.as_bytes();
    let n = bs.len();
    let mut dp: Vec<Vec<(bool, usize, u8)>> = vec![vec![(false, usize::MAX, 0); 2]; n + 1];
    dp[0][0].0 = true;
    dp[0][0].1 = 0;
    dp[0][1].0 = true;
    dp[0][1].1 = 0;
    for i in 0..n {
      let (f, p, b) = dp[i][0];
      if p != usize::MAX {
        dp[i + 1][0] = (f, p + 1, b);
        if b == bs[i] {
          dp[i + 1][1] = (!f, p, b);
        } else if f {
          dp[i + 1][1] = (false, p, bs[i]);
        }
      }
      let (f, p, b) = dp[i][1];
      if p != usize::MAX {
        if p + 1 < dp[i + 1][0].1 {
          dp[i + 1][0] = (f, p + 1, b);
        }
        if b == bs[i] {
          if p < dp[i + 1][1].1 {
            dp[i + 1][1] = (!f, p, b);
          }
        } else if f {
          if p < dp[i + 1][1].1 {
            dp[i + 1][1] = (false, p, bs[i]);
          }
        }
      }
    }
    println!("{} {:?}", s, dp);
  }
}