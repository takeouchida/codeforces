use std::io::{self, BufRead};
use std::cmp::Ordering;
use std::cmp::min;

pub fn lower_bound<T: Ord>(vec: &[T], val: T) -> usize {
  let mut lo = 0;
  let mut hi = vec.len();
  while hi - lo > 0 {
    let p = (hi + lo) / 2;
    if vec[p] < val {
      lo = p + 1;
    } else {
      hi = p;
    }
  }
  return lo;
}

#[derive(PartialEq, Eq, Debug, Copy, Clone, Default, Hash)]
struct Region(usize, usize);

impl PartialOrd for Region {
  fn partial_cmp(&self, other: &Region) -> Option<Ordering> {
    self.0.partial_cmp(&other.0)
  }
}

impl Ord for Region {
  fn cmp(&self, other: &Region) -> Ordering {
    self.0.cmp(&other.0)
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let c: Vec<char> = lines.next().unwrap().unwrap().chars().collect();
    let mut sum_plus = vec![0; n + 1];
    let mut sum_minus = vec![0; n + 1];
    let mut minus_regions = Vec::<Region>::new();
    let mut minus_beg = None;
    for i in 0..n {
      if c[i] == '+' {
        sum_plus[i + 1] = 1 + sum_plus[i];
        sum_minus[i + 1] = sum_minus[i];
        if let Some(j) = minus_beg {
          minus_regions.push(Region(i - j, j));
          minus_beg = None;
        }
      } else {
        sum_plus[i + 1] = sum_plus[i];
        sum_minus[i + 1] = 1 + sum_minus[i];
        if let Some(j) = minus_beg {
          minus_beg = Some(j + 1);
        } else {
          minus_beg = Some(1);
        }
      }
    }
    if let Some(j) = minus_beg {
      minus_regions.push(Region(n - j, j));
    }

    for i in 0..=(n - 1) {
      for j in (i + 1)..=n {
        let count_plus = sum_plus[j] - sum_plus[i];
        let count_minus = sum_minus[j] - sum_minus[i];
        let lo = lower_bound(&minus_regions, Region(i, 0));
        let mut hi = lower_bound(&minus_regions, Region(j, 0));
        if 0 < hi && lo < hi && j < minus_regions[hi - 1].0 + minus_regions[hi - 1].1 {
          hi -= 1;
        }
        let double_1: usize = minus_regions[lo..hi].iter().map(|r| r.1).sum();
      }
    }
  }
}