use std::io::{self, BufRead};
use std::collections::BinaryHeap;
use std::cmp::Ordering;

#[derive(PartialEq, Eq, Debug, Copy, Clone, Default, Hash)]
struct People(usize);

impl PartialOrd for People {
  fn partial_cmp(&self, other: &People) -> Option<Ordering> {
    other.0.partial_cmp(&self.0)
  }
}

impl Ord for People {
  fn cmp(&self, other: &People) -> Ordering {
    other.0.cmp(&self.0)
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let p: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).map(|x: usize| x - 1).collect();
    let mut e: Vec<Vec<usize>> = vec![vec![]; n];
    for i in 0..(n - 1) {
      e[p[i]].push(i);
    }
    let mut c: Vec<usize> = vec![0; n + 1];
    c[0] = 1;
    for i in 0..n {
      c[i + 1] = e[i].len();
    }
    c.sort_by(|x, y| y.cmp(&x));
    let mut pos = 0;
    for i in 0..(n + 1) {
      if c[i] > 0 {
        pos += 1;
      } else {
        break;
      }
    }
    for i in 0..pos {
      let j = pos - i - 1;
      if c[j] < i + 1 {
        c[j] = 0;
      } else {
        c[j] -= i + 1;
      }
    }
    let mut pos2 = 0;
    for i in 0..(n + 1) {
      if c[i] > 0 {
        pos2 += 1;
      } else {
        break;
      }
    }
    if pos2 == 0 {
      println!("{}", pos);
    } else {
      let mut q: BinaryHeap<People> = (0..pos2).map(|i| People(c[i])).collect();
      let mut t = 0;
      loop {
        let mut x = q.pop().unwrap();
        if t < x.0 {
          x.0 -= 1;
          q.push(x);
          t += 1;
        } else {
          break;
        }
      }
      println!("{}", pos + t);
    }
  }
}