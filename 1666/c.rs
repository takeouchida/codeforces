use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let mut p: Vec<(i64, i64)> = vec![];
  for _ in 0..3 {
    let x: Vec<i64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    p.push((x[0], x[1]));
  }
  let mut d: Vec<(i64, (usize, usize))> = vec![];
  for i in 0..3 {
    let j = (i + 1) % 3;
    let q = p[i];
    let r = p[j];
    let l = (r.0 - q.0).abs() + (r.1 - q.1).abs();
    d.push((l, (i, j)));
  }
  d.sort_by(|x, y| x.0.cmp(&y.0));
  let (k1, k2) = d[0].1;
  let (l1, l2) = d[1].1;
  let mut paths: Vec<((i64, i64), (i64, i64))> = vec![];
  if k1 == l2 {
    let (x1, y1) = p[k2];
    let (x2, _ ) = p[k1];
    let (x3, y3) = p[l1];
    paths.push(((x1, y1), (x2, y1)));
    paths.push(((x2, y1), (x2, y3)));
    paths.push(((x2, y3), (x3, y3)));
  } else {
    // k2 == l1
    let (x1, y1) = p[k1];
    let (x2, _ ) = p[k2];
    let (x3, y3) = p[l2];
    paths.push(((x1, y1), (x2, y1)));
    paths.push(((x2, y1), (x2, y3)));
    paths.push(((x2, y3), (x3, y3)));
  }
  println!("{}", paths.len());
  for i in 0..paths.len() {
    let ((x1, y1), (x2, y2)) = paths[i];
    println!("{} {} {} {}", x1, y1, x2, y2);
  }
}