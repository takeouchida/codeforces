use std::io::{self, BufRead};
use std::collections::BTreeSet;

fn dfs(e: &Vec<Vec<usize>>, u: usize, p: &mut Vec<(usize, usize)>, visited: &mut BTreeSet<usize>) {
  visited.insert(u);
  let mut n = 0;
  for v in &e[u] {
    if visited.contains(v) {
      continue;
    }
    n += 1;
    dfs_(e, *v, 1, p, visited);
  }
  if n == 1 {
    p.push((u, 0));
  }
}

fn dfs_(e: &Vec<Vec<usize>>, u: usize, l: usize, p: &mut Vec<(usize, usize)>, visited: &mut BTreeSet<usize>) {
  visited.insert(u);
  let mut is_leaf = true;
  for v in &e[u] {
    if visited.contains(v) {
      continue;
    }
    is_leaf = false;
    dfs_(e, *v, l + 1, p, visited);
  }
  if is_leaf {
    p.push((u, l))
  }
}

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let x: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let n = x[0];
  let m = x[1];
  let s = x[2] - 1;
  let mut e: Vec<Vec<usize>> = vec![vec![]; n];
  for _ in 0..m {
    let x: Vec<usize> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let u = x[0] - 1;
    let v = x[1] - 1;
    e[u].push(v);
    e[v].push(u);
  }
  if e[s].len() < 2 {
    println!("Impossible");
    return;
  }
  let mut p = vec![];
  let mut visited = BTreeSet::new();
  dfs(&e, s, &mut p, &mut visited);
  println!("{:?}", p);
}