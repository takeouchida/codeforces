use std::io::{self, BufRead};
use std::cmp::{min, max};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let x: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let n = x[0] - 1;
    let m = x[1] - 1;
    let lo = min(n, m);
    let hi = max(n, m);
    let a = (hi - lo) / 2;
    let b = (hi - lo) % 2;
    if lo == 0 && hi >= 2 {
      println!("-1");
    } else {
      let ans = lo * 2 + a * 4 + b;
      println!("{}", ans);
    }
  }
}