use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let x: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let n = x[0];
    let m = x[1];
    let a: Vec<u64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let hi = a.iter().max().unwrap();
    let lo = a.iter().min().unwrap();
    let total = a.len() as u64 + a.iter().sum::<u64>() + hi - lo;
    println!("{}", if total <= m { "YES" } else { "NO" });
  }
}