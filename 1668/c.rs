use std::io::{self, BufRead};
use std::cmp::min;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
  let a: Vec<i64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
  let mut ans = i64::MAX;
  for i in 0..n {
    let mut count = 0;
    let mut prev = 0;
    for j in (i + 1)..n {
      let c = prev / a[j] + 1;
      count += c;
      prev = c * a[j];
    }
    if i > 0 {
      let mut prev = 0;
      for j in (0..i).rev() {
        let c = prev / a[j] + 1;
        count += c;
        prev = c * a[j];
      }
    }
    ans = min(ans, count);
  }
  println!("{}", ans);
}