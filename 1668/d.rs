use std::io::{self, BufRead};
use std::cmp::max;

fn main() {
  let stdin = io::stdin();
  let mut lines = stdin.lock().lines();
  let t: usize = lines.next().unwrap().unwrap().parse().unwrap();
  for _ in 0..t {
    let n: usize = lines.next().unwrap().unwrap().parse().unwrap();
    let a: Vec<i64> = lines.next().unwrap().unwrap().split(' ').map(|x| x.parse().unwrap()).collect();
    let mut x = a[0]; // rightmost piece
    let mut y = 0; // split
    let mut z = 0; // not split
    for i in 1..n {
      if x + a[i] > 0 {
        y += 1;
        z = max(y, z) + 1;
        x += a[i];
      } else if x + a[i] == 0 {
        if x > 0 {
          // y = 
        }
      }
    }
  }
}