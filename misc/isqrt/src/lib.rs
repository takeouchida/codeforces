pub fn isqrt(n: u64) -> u64 {
  let mut x0 = n / 2;
  if x0 == 0 {
    return n;
  }
  let mut x1 = (x0 + n / x0) / 2;
  while x1 < x0 {
    x0 = x1;
    x1 = (x0 + n / x0) / 2;
  }
  x0
}

#[cfg(test)]
mod tests {
  use super::*;
  #[test]
  fn it_works() {
    assert_eq!(0, isqrt(0));
    assert_eq!(1, isqrt(1));
    assert_eq!(1, isqrt(2));
    assert_eq!(1, isqrt(3));
    assert_eq!(2, isqrt(4));
    assert_eq!(2, isqrt(8));
    assert_eq!(3, isqrt(9));
    assert_eq!(3, isqrt(15));
    assert_eq!(4, isqrt(16));
    assert_eq!(9, isqrt(99));
    assert_eq!(10, isqrt(100));
    assert_eq!(10, isqrt(101));
    assert_eq!(99, isqrt(9999));
    assert_eq!(100, isqrt(10000));
  }
}
